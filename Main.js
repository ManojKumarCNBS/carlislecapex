//# sourceURL=main.js
function init() {

	fabSetCheckForChanges(false);
	fabSetFileUploaderByKeyURL(getControl('NarrativeUpload'));
	fabSetFileUploaderByKeyURL(getControl('QuotationUpload'));
	fabSetFileUploaderByKeyURL(getControl('SalesUpload'));
	fabSetFileUploaderByKeyURL(getControl('OtherUpload'));
	fabSetSimpleFileUploaderByKeyURL(getControl("leaseFileUploader"));
	fabSetSimpleFileUploaderByKeyURL(getControl("CFPFileUploader"));
	TitleHide(); //function called in onload page to hide the duplicate section titles.
	dateDisplay(); //function called in onload page to display date in header section.
	var Level = getField("CurrentApprovalLevel");
	//var ActionLevel = getField("CurrentLevelAction");

	var Companies = getField("SavedCompanies");
	var fields = Companies.split(',');
	if (Companies !== "") {
		for (i = 0; i < (fields.length - 1); i++) {
			getControl("RequestInfomultiInput").addToken(new sap.m.Token({
				text: fields[i]
			}));
		}
	}


	//if (Level < 6) {

	var Profit = [{
			ProfitLabel: "Pre-Tax Profit"
		},
		{
			ProfitLabel: "After-Tax Profit"
		}
	]
	IncomeAndExpenseSectionSetup("IncomeTableEC", 1);
	IncomeAndExpenseSectionSetup("IncomeTableWC", 1);
	IncomeAndExpenseSectionSetup("IncomeTableBC", 1);
	IncomeAndExpenseSectionSetup("ExpenseTableEC", 3);
	IncomeAndExpenseSectionSetup("ExpenseTableWC", 3);
	IncomeAndExpenseSectionSetup("ExpenseTableBC", 3);
	//}

	if (Level === 0) {

		CashFlowPaybackTableSetup();
	}
	CFPOnLoad();
	//this is done so that the panels are not hidden until all the page is fully setUploadEnabled
	//other the javascript that sets up those tables will not function properly
	setTimeout(function() {
		setPanels();
	}, 3000);
	CFFlag = "EC";
	ICFlag = "EC";
	setChart();
	lockCRL();
}

function setPanels() {
	setField("ExpectedCaseEdit", true);
	setField("WorstCaseEdit", false);
	setField("BestCaseEdit", false);
	setField("Utility/DataLoading", false);

	if (getField("EditForm") === false) {

		getControl("leaseFileUploader").setEnabled(false);
		getControl("NarrativeUpload").setUploadEnabled(false);
		getControl("QuotationUpload").setUploadEnabled(false);
		getControl("SalesUpload").setUploadEnabled(false);
		getControl("OtherUpload").setUploadEnabled(false);
		var NarrativeUpload = getControl("NarrativeUpload").getItems().length;
		if (NarrativeUpload > 0) {
			var Nlength = getControl("NarrativeUpload").getItems().length;
			for (i = 0; i < Nlength; i++) {
				getControl("NarrativeUpload").getItems()[i].setEnableEdit(false);
				getControl("NarrativeUpload").getItems()[i].setEnableDelete(false);
			}
		}

		var QuotationUpload = getControl("QuotationUpload").getItems().length;
		if (QuotationUpload > 0) {
			var Qlength = getControl("QuotationUpload").getItems().length;
			for (i = 0; i < Qlength; i++) {
				getControl("QuotationUpload").getItems()[i].setEnableEdit(false);
				getControl("QuotationUpload").getItems()[i].setEnableDelete(false);
			}
		}

		var SalesUpload = getControl("SalesUpload").getItems().length;
		if (SalesUpload > 0) {
			var Slength = getControl("SalesUpload").getItems().length;
			for (i = 0; i < Slength; i++) {
				getControl("SalesUpload").getItems()[i].setEnableEdit(false);
				getControl("SalesUpload").getItems()[i].setEnableDelete(false);
			}

		}

		var OtherUpload = getControl("OtherUpload").getItems().length;
		if (OtherUpload > 0) {
			var Olength = getControl("OtherUpload").getItems().length;
			for (i = 0; i < Olength; i++) {
				getControl("OtherUpload").getItems()[i].setEnableEdit(false);
				getControl("OtherUpload").getItems()[i].setEnableDelete(false);
			}
		}
	}

	fabSetCheckForChanges(true);

	if(getField("CashFlowEdit1") === true) {
	    getControl("CFPFileUploader").setEnabled(getField("EditForm"));
	}
}





function IncomeAndExpenseSectionSetup(tableName, tableType) {
	var oTable = getControl(tableName);
	var lModel = getTableModel(oTable);
	var path = "";
	if (oTable.getMetadata().getName() !== "sap.ui.table.Table") {
		path = oTable.getBindingInfo("items").path;
	} else {
		path = oTable.getBindingInfo("rows").path;
		if (path.indexOf("/") !== 0) {
			path = '/' + path;
		}
	}
	if (lModel.getProperty(path) === undefined) {
		lModel.setProperty(path, []);
	}

	var row = getTableRow(tableName, 0);

	//we don't want to add rows if they already exist
	if (row === undefined) {
		if (tableType === 1) {
			addRowToIncomeAndExpenseModel(tableName, lModel, path, "Trade Sales", 0);


			addRowToIncomeAndExpenseModel(tableName, lModel, path, "Inter Company Sales", 1);
		} else {
			addRowToIncomeAndExpenseModel(tableName, lModel, path, "Cost of Sales", 0);
			addRowToIncomeAndExpenseModel(tableName, lModel, path, "Tax Depreciation", 1);
			addRowToIncomeAndExpenseModel(tableName, lModel, path, "Project Expense", 2);
		}
	}
	SetIEDisabledFields(tableName, tableType);

}

function addRowToIncomeAndExpenseModel(tableName, lModel, path, category, index) {
	var oData = {
		Category: category,
		Description: "",
		Year0: "0",
		Year1: "0",
		Year2: "0",
		Year3: "0",
		Year4: "0",
		Year5: "0",
		Year6: "0",
		Year7: "0",
		Year8: "0",
		Year9: "0",
		Year10: "0",
		Total: "0"
	}
	lModel.getProperty(path).push(oData);
	lModel.refresh();
}

// To hide the duplicate titles on the each section.
function TitleHide() {
	getControl("_view1--NarrativeSection").setShowTitle(false);
	getControl("_view1--CapitalRequestListSection").setShowTitle(false);
	getControl("_view1--IncomeExpenseSection").setShowTitle(false);
	getControl("_view1--CashflowPaybackSection").setShowTitle(false);
	getControl("_view1--Approvers").setShowTitle(false);
	getControl("_view1--WorkflowSection").setShowTitle(false);
}
// Functionality used in the Cash flow and payback section on Alternate Spreedsheet.
function CheckBoxTrue() {
	setField("CashFlowEdit", false);
	setField("CashFlowEdit1", true);
}


function CheckBoxFalse() {

	setField("CashFlowEdit", true);
	setField("CashFlowEdit1", false);
	BuildCFPData(ICFlag);
}

// Live change functionality used in request information section on project title input.To display live changes on Header section Title.
function TitleLiveChange(evt) {

	var title = evt.getSource().getValue();
	UpdateProjectTitle(title);
}

// delete this later delete delete this this
// delete this line

function UpdateProjectTitle(title) {

	if (title === null || title.length === 0) {
		title = getField("ProjectTitle");
	}

    var crlrows = getField("CapitalRequestList");
    let companies = crlrows.map(a => a.Company);
    let plants = crlrows.map(a => a.Plant);

    const dxtComs = [...new Set(companies)];
    const dxtPlts = [...new Set(plants)];

    var plant = '';
    var company = '';

    if(dxtPlts.length > 1) plant = "Multi-Plant";
    if(dxtComs.length > 1) company = "Multi-Company";

	var oRow = getTableRow("CapitalRequestListTable", 0);

	if (oRow !== undefined) {
		var p = getControlOfTableRow("CapitalRequestListTable", 0, 3);
		if (p !== undefined) {
			if(plant.length === 0) plant = p._getSelectedItemText();
			if (plant.length > 0) {
				title = plant + " - " + title;
			}
		}

		var c = getControlOfTableRow("CapitalRequestListTable", 0, 2);
		if (c !== undefined) {
			if(company.length === 0) company = c._getSelectedItemText();
			if (company.length > 0) {
				title = company + " - " + title;
			}
		}
	}

	getControl("HeaderMainTitle").setText(title);
}

// Switch Selection functionality used in Request Information for Asset Disposition to display Disposal asset.
function OnSelectOn() {

	var Selection = getControl("switchSelect").getState();

	if (Selection === true) {
		setField("RequestInfoEdit", true);
	} else if (Selection === false) {
		setField("RequestInfoEdit", false);
	}
}

// Add Row functionality on the Expected Case Income Table.
function addRowEC() {

	var oItem = new sap.m.ColumnListItem({
		cells: [new sap.m.Select(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(),
			new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Button({
				icon: "sap-icon://decline",
				type: "Transparent",
				press: function(evt) {
					var oTable = getControl("IncomeTableEC");
					oTable.removeItem(evt.getSource().getParent());
				}
			})
		]
	});

	var oTable = getControl("IncomeTableEC");
	oTable.addItem(oItem);
}
function cnbsIncomeTableRemoveButtonPressed(evt) {
	if (getField("deleteICItem") === true || getField("deleteICItemWC") === true || getField("deleteICItemBC") === true) {
		setField("deleteICItem", false);
		setField("deleteICItemWC", false);
		setField("deleteICItemBC", false);
		return;
	}
	if (ICFlag === "EC") {
		setField("deleteICItem", true);
		setField("deleteICItemWC", false);
		setField("deleteICItemBC", false);
	} else if (ICFlag === "WC") {
		setField("deleteICItemWC", true);
		setField("deleteICItem", false);
		setField("deleteICItemBC", false);
	} else if (ICFlag === "BC") {
		setField("deleteICItemBC", true);
		setField("deleteICItem", false);
		setField("deleteICItemWC", false);
	}
	var ICTableName = evt.getSource().getParent().getParent().sId.split("--");
	var oControl = getControl(ICTableName[1]);
	var fixedRows = 0;
	if(ICTableName[1].includes("Expense")) fixedRows = 2
	else if(ICTableName[1].includes("Income")) fixedRows = 1
	for (var i = 0; i < oControl.getItems().length; i++) {
		var ICTableRow = getTableRow(ICTableName[1], i);
		setTableRowField(ICTableRow, "deleteFlag", true);
		if (i <= fixedRows) {
			setTableRowField(ICTableRow, "enableFlag", false);
		} else {
			setTableRowField(ICTableRow, "enableFlag", true);
		}
	}
}
function callDeleteAction(evt) {
	debugger;
	var context = evt.oSource.getBindingContext();
	var path = context.getPath();
	var arr = path.split("/");
	var data = getField(arr[1]);
	data.splice(arr[2], 1);
	setField(arr[1], data);
}
// Remove row functionality on the Expected Case Income Table.
function removeRowEC(evt) {
	var buttonState = evt.getSource().getPressed();

	if (buttonState === true) {

		setField("IncomeColumnEdit", true);

	} else if (buttonState === false) {

		setField("IncomeColumnEdit", false);
	}
}

// Add Row functionality on the Worst Case Income Table.
function addRowWC() {

	var oItem = new sap.m.ColumnListItem({
		cells: [new sap.m.Select(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(),
			new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Button({
				icon: "sap-icon://decline",
				type: "Transparent",
				press: function(evt) {
					var oTable = getControl("IncomeTableWC");
					oTable.removeItem(evt.getSource().getParent());
				}
			})
		]
	});

	var oTable = getControl("IncomeTableWC");
	oTable.addItem(oItem);
}

// Remove row functionality on the Worst Case Income Table.
function removeRowWC(evt) {


	var buttonState = evt.getSource().getPressed();

	if (buttonState === true) {

		setField("IncomeColumnEdit", true);
	} else if (buttonState === false) {

		setField("IncomeColumnEdit", false);
	}
}

// Add Row functionality on the Best Case Income Table.
function addRowBC() {

	var oItem = new sap.m.ColumnListItem({
		cells: [new sap.m.Select(), new sap.m.Input(), new sap.m.Input({
				type: "Number"
			}), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(),
			new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Input(), new sap.m.Button({
				icon: "sap-icon://decline",
				type: "Transparent",
				press: function(evt) {
					var oTable = getControl("IncomeTableBC");
					oTable.removeItem(evt.getSource().getParent());
				}
			})
		]
	});

	var oTable = getControl("IncomeTableBC");
	oTable.addItem(oItem);
}


// Remove row functionality on the Best Case Income Table.
function removeRowBC(evt) {


	var buttonState = evt.getSource().getPressed();

	if (buttonState === true) {

		setField("IncomeColumnEdit", true);
	} else if (buttonState === false) {

		setField("IncomeColumnEdit", false);
	}
}



// Displaying the date in the Header Section.
function dateDisplay() {

	var monthname = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
	];

	var setDate = false;
	var createdDate = getField("Createddate");
	if (createdDate === undefined) {
		createdDate = new Date();
		setdate = true;
	}

	var year = getField("Createddate").getFullYear();
	var month = getField("Createddate").getMonth();
	var date = getField("Createddate").getDate();

	var TodayDate = monthname[month] + " " + date + "," + " " + year;

	if (getField("date") === "") setField("date", TodayDate);
}



function UpdateIECalculatedRows() {
	var toTable = "ExpenseTable" + ICFlag;
	var prjTaxDepRow = getTableRow(toTable, 1);
	var prjExpRow = getTableRow(toTable, 2);
	setTableRowField(prjTaxDepRow, "Year0", 0);
	setTableRowField(prjExpRow, "Year0", 0);
	setTableRowField(prjTaxDepRow, "Year1", amountFormatter(getFieldAsNumeric("CapitalTotal")));
	setTableRowField(prjExpRow, "Year0", amountFormatter(getFieldAsNumeric("ExpenseTotal")));
	setTableRowField(prjTaxDepRow, "Total", amountFormatter(getFieldAsNumeric("CapitalTotal")));
	setTableRowField(prjExpRow, "Total", amountFormatter(getFieldAsNumeric("ExpenseTotal")));
	for (var i = 2; i < 11; i++) {
		setTableRowField(prjTaxDepRow, "Year" + i.toString(), 0);
		setTableRowField(prjExpRow, "Year" + i.toString(), 0);
	}
}



// Income and expense Section.
var ICFlag;

function setScenario(key) {
	ICFlag = key;
	CFFlag = ICFlag;
	var toTable = "ExpenseTable" + key;
	getControl("CFSelect").setSelectedKey(CFFlag);
	setField("Utility/DataLoading", true);

	//must occur before the rest of this function
	UpdateIECalculatedRows();

	if (key === "EC") {
		setField("ExpectedCaseEdit", true);
		setField("WorstCaseEdit", false);
		setField("BestCaseEdit", false);
		ECRefreshed();
	} else if (key === "WC") {
		setField("ExpectedCaseEdit", false);
		setField("WorstCaseEdit", true);
		setField("BestCaseEdit", false);
		WCRefreshed();
	} else if (key === "BC") {
		setField("ExpectedCaseEdit", false);
		setField("WorstCaseEdit", false);
		setField("BestCaseEdit", true);
		BCRefreshed();
		IncomeBCRefreshed();
		ExpenseBCRefreshed();
	}

	setField("Utility/DataLoading", false);
	updateCFRows();

}

function OnSegmentedSelect(evt) {

	var key = evt.getSource().getSelectedKey();
	setScenario(key);

}


function RefreshWorkflow() {
	setTimeout(function() {
		fabUpdateProcessFlowDiagram();
	}, 1000);
}

//To make currency column value as base currency value in Capital request list.
function AddButtonPressed(evt) {

	var itemAdded = evt.getSource().getParent().getParent().getItems().length;
	var i = itemAdded - 1;
	var BaseCurrency = getField("HeaderCurrency");
	evt.getSource().getParent().getParent().getItems()[i].getCells()[6].setValue("0");
	evt.getSource().getParent().getParent().getItems()[i].getCells()[7].setValue("0");
	evt.getSource().getParent().getParent().getItems()[i].getCells()[0].setValue("1");

	if (BaseCurrency === "" || BaseCurrency === null) {
		evt.getSource().getParent().getParent().getItems()[i].getCells()[5].setValue("USD");
	} else {
		evt.getSource().getParent().getParent().getItems()[i].getCells()[5].setValue(BaseCurrency);
	}
}

//To get Company search value in Capital request list.
function CompanySelect(evt) {


	var CompanyCode = evt.getSource().getValue();
	setField("BUKRS", CompanyCode);
	evt.getSource().getParent().getCells()[3].setValue("")
	evt.getSource().getParent().getCells()[4].setValue("")
}

//to update ACT type in Header section when there is a change in Request info ACT type.
function changeActype(evt) {

	var Ace = evt.getParameters().item.getText();
	setField("Actype", Ace);
}

function CRLCurrencyConversion(evt) {

	var oPayload = {};

	var actionParameters = {
		showBusyDialog: true,
		showSuccessMessage: true,
		successFunction: successCRL
	};

	fabPerformAction("ConvertCurrency", oPayload, actionParameters, null);

}

function successCRL(evt) {

	setChart();
	var Length = getField("CapitalRequestList").length;
	var Message = [];
	for (i = 0; i < Length; i++) {

		var Maint = getField("CapitalRequestList")[i].ExhangeRateMaint;

		if (Maint === 'N') {
			var FromCurrecy = getField("CapitalRequestList")[i].CurrencyCode;
			var ToCurrency = getField("HeaderCurrency");
			MessageText = "\n" + FromCurrecy + " to " + ToCurrency;
			Message.push(MessageText);
		}
	}
	var MessageLength = Message.length;
	if (MessageLength > 0) {
		sap.m.MessageBox.alert("Please maintain Exchange rates for the following: " + Message);
	}
	//Update Capital and Expense calculated rows
	UpdateIECalculatedRows();
	ExpenseColumnTotal(ICFlag);

	UpdateProjectTitle(null);

	UpdateCFPACETotal();
	BuildCFPData(ICFlag);
	//setField("EditForm", true); // overriding bug that keeps setting rows to locked.
}

function setChart() {
	var totalRequested = getField("RequestTotal");
	var totalCapital = getField("CapitalTotal");
	var totalExpense = getField("ExpenseTotal");
	var x = amountFormatter(getField("RequestTotal"));
	getControl("chartCapital").setProperty("displayValue", amountFormatter(totalCapital));
	getControl("chartExpense").setProperty("displayValue", amountFormatter(totalExpense));
	getControl("chartTotal").setProperty("displayValue", x);
	var y = getControl("chartRequest");
	y.setSubheader(x);
}


function IEAddingColumns(evt) {

	var year0 = parseInt((evt.getSource().getParent().getCells()[2].getValue()).replace(/\,/g, ''), 10);

	var Year0 = isNaN(year0);
	if (Year0 === true) {

		year0 = 0;
	}

	var year1 = parseInt((evt.getSource().getParent().getCells()[3].getValue()).replace(/\,/g, ''), 10);
	var Year1 = isNaN(year1);
	if (Year1 === true) {

		year1 = 0;
	}
	var year2 = parseInt((evt.getSource().getParent().getCells()[4].getValue()).replace(/\,/g, ''), 10);
	var Year2 = isNaN(year2);
	if (Year2 === true) {

		year2 = 0;
	}
	var year3 = parseInt((evt.getSource().getParent().getCells()[5].getValue()).replace(/\,/g, ''), 10);
	var Year3 = isNaN(year3);
	if (Year3 === true) {

		year3 = 0;
	}
	var year4 = parseInt((evt.getSource().getParent().getCells()[6].getValue()).replace(/\,/g, ''), 10);
	var Year4 = isNaN(year4);
	if (Year4 === true) {

		year4 = 0;
	}
	var year5 = parseInt((evt.getSource().getParent().getCells()[7].getValue()).replace(/\,/g, ''), 10);
	var Year5 = isNaN(year5);
	if (Year5 === true) {


		year5 = 0;

	}
	var year6 = parseInt((evt.getSource().getParent().getCells()[8].getValue()).replace(/\,/g, ''), 10);
	var Year6 = isNaN(year6);
	if (Year6 === true) {

		year6 = 0;
	}
	var year7 = parseInt((evt.getSource().getParent().getCells()[9].getValue()).replace(/\,/g, ''), 10);
	var Year7 = isNaN(year7);
	if (Year7 === true) {

		year7 = 0;
	}
	var year8 = parseInt((evt.getSource().getParent().getCells()[10].getValue()).replace(/\,/g, ''), 10);
	var Year8 = isNaN(year8);
	if (Year8 === true) {

		year8 = 0;
	}
	var year9 = parseInt((evt.getSource().getParent().getCells()[11].getValue()).replace(/\,/g, ''), 10);
	var Year9 = isNaN(year9);
	if (Year9 === true) {

		year9 = 0;
	}
	var year10 = parseInt((evt.getSource().getParent().getCells()[12].getValue()).replace(/\,/g, ''), 10);
	var Year10 = isNaN(year10);
	if (Year10 === true) {

		year10 = 0;
	}

	var Total = year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 + year9 + year10;

	evt.getSource().getParent().getCells()[13].setValue(Total);
}


function successCheck(evt) {
    // needed for validation of number quotations attachments required
	var QuotationLength = getControl("QuotationUpload").getItems().length;
	setField("AttachmentsLength", QuotationLength);
	// needed for validation of number narrative attachments required
	var narLen = getControl("NarrativeUpload").getItems().length;
	setField("AttachmentNarrativeLength", narLen);
	var Length = getField("CapitalRequestList").length;
	var Message = [];
	var Com = [];
	for (i = 0; i < Length; i++) {

		var Maint = getField("CapitalRequestList")[i].ExhangeRateMaint;

		if (Maint === 'N') {
			var FromCurrecy = getField("CapitalRequestList")[i].CurrencyCode;
			var ToCurrency = getField("HeaderCurrency");
			MessageText = "\n" + FromCurrecy + " to " + ToCurrency;
			Message.push(MessageText);
		}
	}
	var MessageLength = Message.length;
	if (MessageLength > 0) {
		sap.m.MessageBox.alert("Please maintain Exchange rates for the following: " + Message);
	} else {
		continueAction(evt);

	}
}

function CloseDialog() {
	CRLCurrencyConversion();
	getControl("CapitalRequestExcelDropDialog").close();
}


function BeforeSave(evt) {


	var Companies = [];
	var selectedCompanies = "";
	var previousSelected = getControl("RequestInfomultiInput").getValue();
	var previousSelectedItems = previousSelected.split(",");
	var Length = getControl("RequestInfomultiInput").getTokens().length;
	if (Length > 1) {
		setField("SapOrderSwitch", false);
	}

	var Length = getControl("RequestInfomultiInput").getTokens().length;
	for (i = 0; i < Length; i++) {
		var Com = [];
		Com["Text"] = getControl("RequestInfomultiInput").getTokens()[i].getText();
		var removedCompanies = GetRemovedCompanies(evt.mParameters.removedTokens);

		if (removedCompanies.indexOf(getControl("RequestInfomultiInput").getTokens()[i].getText()) === -1) {
			Companies.push(Com);
			selectedCompanies = selectedCompanies + Com.Text + ",";
		}
	}


	setField("CompanySelected", Companies);

	selectedCompanies = previousSelected + selectedCompanies;

	setField("SavedCompanies", selectedCompanies);

	var AllSelectedCompanies = selectedCompanies.replace(/,(?=\s*$)/, '');
	AllSelectedCompanies = AllSelectedCompanies.replace(/,/g, ", ");
	setField("AllSelectedCompanies", AllSelectedCompanies);


	var oPayload = {};

	var actionParameters = {
		showBusyDialog: true,
		showSuccessMessage: true,
		successFunction: success
	};

	fabPerformAction("CompanySelected", oPayload, actionParameters, null);
}

function GetRemovedCompanies(tokens) {

	var rc = [];
	if (tokens === undefined || tokens.length === 0) return rc;

	for (var i = 0; i < tokens.length; i++) {
		rc.push(tokens[i].mProperties.text);
	}

	return rc;
}

function FixedAssetSelected(evt) {

	var SelectedAssetTypeText = evt.getParameters().selectedItem.getText();
	setField("AssetTypeDescription", SelectedAssetTypeText);
	var oPayload = {};

	var actionParameters = {
		showBusyDialog: true,
		showSuccessMessage: true,
		successFunction: FixedAssetSuccess
	};

	fabPerformAction("AssetSelected", oPayload, actionParameters, null);
}

function FixedAssetSuccess() {

}

function IncomeTableRefreshed(tableName) {

	var ctl = getControl(tableName).getItems()[0].getCells()[0].getSelectedItem();
	if (ctl !== null) {
		setCategoryForFixedRow(tableName, "Trade Sales", 0);
		setCategoryForFixedRow(tableName, "Inter Company Sales", 1);
		SetIEDisabledFields(tableName, 1);
	}
}

function ExpenseTableRefreshed(tableName) {

	var ctl = getControl(tableName).getItems()[0].getCells()[0].getSelectedItem();
	if (ctl !== null) {
		setCategoryForFixedRow(tableName, "Cost of Sales", 0);
		setCategoryForFixedRow(tableName, "Tax Depreciation", 1);
		setCategoryForFixedRow(tableName, "Project Expense", 2);
		SetIEDisabledFields(tableName, 3);
	}
}

function setCategoryForFixedRow(tableName, category, index) {
	getControl(tableName).getItems()[index].getCells()[0].getSelectedItem().setText(category);
	getControl(tableName).getItems()[index].getCells()[0].setEnabled(false);
}

function SetIEDisabledFields(tableName, tableType) {
	getControl(tableName).getItems()[0].getCells()[0].setEnabled(false);
	getControl(tableName).getItems()[1].getCells()[0].setEnabled(false);

	if (tableType !== 1) { // expense table
		for (var i = 0; i < 13; i++) {

			getControl(tableName).getItems()[1].getCells()[i].setEnabled(false);
			getControl(tableName).getItems()[2].getCells()[i].setEnabled(false);
		}
	}
}

function IncomeECRefreshed() {
	IncomeTableRefreshed("IncomeTableEC");
	if (getField("Utility/DataLoading") === true) return;
	ECRefreshed();
}

function IncomeWCRefreshed() {
	IncomeTableRefreshed("IncomeTableWC");
	if (getField("Utility/DataLoading") === true) return;

	WCRefreshed();
}

function IncomeBCRefreshed() {
	IncomeTableRefreshed("IncomeTableBC");
	if (getField("Utility/DataLoading") === true) return;

	BCRefreshed();
}

function DeductionsECRefreshed() {
	if (getField("Utility/DataLoading") === true) return;
	ECRefreshed();
}

function DeductionsWCRefreshed() {
	if (getField("Utility/DataLoading") === true) return;
	WCRefreshed();
}

function DeductionsBCRefreshed() {

	if (getField("Utility/DataLoading") === true) return;
	BCRefreshed();
}

function ExpenseECRefreshed() {
	ExpenseTableRefreshed("ExpenseTableEC");
	if (getField("Utility/DataLoading") === true) return;

	ECRefreshed();
}

function ExpenseWCRefreshed() {
	ExpenseTableRefreshed("ExpenseTableWC");
	if (getField("Utility/DataLoading") === true) return;

	WCRefreshed();
}

function ExpenseBCRefreshed() {
	ExpenseTableRefreshed("ExpenseTableBC");
	if (getField("Utility/DataLoading") === true) return;

	BCRefreshed();
}


function OtherExpenseECRefreshed() {

	if (getField("Utility/DataLoading") === true) return;
	ECRefreshed();
}

function OtherExpenseWCRefreshed() {

	if (getField("Utility/DataLoading") === true) return;
	WCRefreshed();
}

function OtherExpenseBCRefreshed() {

	if (getField("Utility/DataLoading") === true) return;
	BCRefreshed();
}


function ECRefreshed() {
	IncomeECColumnTotal();
	DeductionsECColumnTotal();
	ExpenseECColumnTotal();
	OtherExpenseECColumnTotal();
}

function BCRefreshed() {

	IncomeBCColumnTotal();
	DeductionsBCColumnTotal();
	ExpenseBCColumnTotal();
	OtherExpenseBCColumnTotal();
}


function WCRefreshed() {
	IncomeWCColumnTotal();
	DeductionsWCColumnTotal();
	ExpenseWCColumnTotal();
	OtherExpenseWCColumnTotal();
}


function getFieldAsNumeric(fieldName) {
	var f = -1;
	var rawVal = getField(fieldName).toString();

	if (rawVal === undefined || rawVal === "") return 0;

	if (rawVal.indexOf("(") === -1) f = 1;

	var t = parseInt(rawVal.replace(/\,|\(|\)/g, ''));
	return isNaN(t) ? 0 : t * f;
}


function parseIntZero(val) {
	var t = parseInt(val.toString().replace(/\,|\(|\)/g, ''));
	return isNaN(t) ? 0 : t;
}


function CalculateNetSales(sc) {
	for (var i = 0; i <= 10; i++) {
		var ns = (getFieldAsNumeric(sc + "year" + i.toString()) - getFieldAsNumeric("D" + sc + "year" + i.toString())); // net sales
		setField("NetSales" + sc + "year" + i.toString(), amountFormatter(ns));
	}
	var NetSalesTotal = parseInt(getFieldAsNumeric(sc + "Total")) - parseInt(getFieldAsNumeric("D" + sc + "Total"));
	setField("NetSales" + sc + "Total", amountFormatter(NetSalesTotal));
}

function CalculateGrossMargin(sc) {
	for (var i = 0; i <= 10; i++) {
		var gm = (getFieldAsNumeric("NetSales" + sc + "year" + i.toString()) - getFieldAsNumeric("E" + sc + "year" + i.toString()));
		setField("GrossMargin" + sc + "year" + i.toString(), amountFormatter(gm));
	}
	var gmTotal = parseInt(getFieldAsNumeric("NetSales" + sc + "Total")) - parseInt(getFieldAsNumeric("E" + sc + "Total"));
	setField("GrossMargin" + sc + "Total", amountFormatter(gmTotal));
}

function CalculatePreTaxProfit(sc) {
	for (var i = 0; i <= 10; i++) {
		var ptp = (getFieldAsNumeric("GrossMargin" + sc + "year" + i.toString()) - getFieldAsNumeric("OE" + sc + "year" + i.toString()));
		setField("PreTaxProfit" + sc + "year" + i.toString(), amountFormatter(ptp));
	}
	var ptpTotal = parseInt(getFieldAsNumeric("GrossMargin" + sc + "Total")) - parseInt(getFieldAsNumeric("OE" + sc + "Total"));
	setField("PreTaxProfit" + sc + "Total", amountFormatter(ptpTotal));
}



function CalculateAfterTaxProfit(sc) {
	for (var i = 0; i <= 10; i++) {
		var ptp = getFieldAsNumeric("PreTaxProfit" + sc + "year" + i.toString());
		var atp = (1 - (parseInt(getField("CurrentTaxRate"))) / 100) * ptp;
		setField("AfterTaxProfit" + sc + "year" + i.toString(), amountFormatter(Math.round(atp)));
	}

	var PreTaxProfitTotal = getFieldAsNumeric("PreTaxProfit" + sc + "Total");
	var AfterTaxProfitTotal = (1 - (parseInt(getField("CurrentTaxRate"))) / 100) * PreTaxProfitTotal;
	setField("AfterTaxProfit" + sc + "Total", amountFormatter(Math.round(AfterTaxProfitTotal)));
}


function IncomeECColumnTotal() {
	IncomeColumnTotal("EC");

}

function IncomeWCColumnTotal() {
	IncomeColumnTotal("WC");
}

function IncomeBCColumnTotal() {
	IncomeColumnTotal("BC");
}

function DeductionsECColumnTotal() {
	DeductionsColumnTotal("EC");
}

function DeductionsWCColumnTotal() {
	DeductionsColumnTotal("WC");
}

function DeductionsBCColumnTotal() {
	DeductionsColumnTotal("BC");
}




function ExpenseECColumnTotal() {

	ExpenseColumnTotal("EC");
}

function ExpenseWCColumnTotal() {
	ExpenseColumnTotal("WC");
}

function ExpenseBCColumnTotal() {
	ExpenseColumnTotal("BC");
}



function OtherExpenseECColumnTotal() {
	OtherExpenseColumnTotal("EC");
}

function OtherExpenseWCColumnTotal() {
	OtherExpenseColumnTotal("WC");
}

function OtherExpenseBCColumnTotal() {
	OtherExpenseColumnTotal("BC");
}


// I need to add stuff to DeleteLeaseFile//due to bug in FAB editor
function onUserSelect(evt) {
	var app = evt.getSource();
	var bname = app.getValue();
	var approversList = getField("Approvers");
	var count = 0;
	if (approversList.length > 0) {
		for (i = 0; i < approversList.length; i++) {
			if (approversList[i].bname === bname) {
			    count++;
			}
		}
	}

	if(count > 1) { //duplicate approver
		app.setValueState(sap.ui.core.ValueState.Error);
		app.setValueStateText(bname + " already added as an approver");
		return;
	}
	var oPayload = {};

	var actionParameters = {
		showBusyDialog: true,
		showSuccessMessage: true,
		successFunction: successUser()
	};

	fabPerformAction("ApproverUserSelect", oPayload, actionParameters, null);
}

function successUser() {}

function CashFlowPaybackTableSetup() {
	debugger;
	var Payback = [{
			Description: "After-Tax Net Profit",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		},
		{
			Description: "Tax Depreciation Addback (+)",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		},
		{
			Description: "Capital Expenditures (-)",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		},
		{
			Description: "Disposed Value of Equipment (+)",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		},
		{
			Description: "Changes in Working Capital:"
		},
		{
			// 			Description: "&ensp;&ensp;&ensp;Inventory (-)"
			Description: "- Inventory (-)",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		},
		{
			// 			Description: "&ensp;&ensp;&ensp;Receivables (-)"
			Description: "- Receivables (-)",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		},
		{
			// 			Description: "&ensp;&ensp;&ensp;Payables  (-)"
			Description: "- Payables (-)",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		},
		{
			Description: "Residual Value",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		},
		{
			Description: "Annual Net Cash Flow",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		},
		{
			Description: "Cumulative Cash Flow",
			Year0: "0",
			Year1: "0",
			Year2: "0",
			Year3: "0",
			Year4: "0",
			Year5: "0",
			Year6: "0",
			Year7: "0",
			Year8: "0",
			Year9: "0",
			Year10: "0"
		}
	]
	setField("CashFlowPayback", Payback);
}

function CFPOnLoad() {
	var toTable = "CashFlowPaybackTable";
	for (i = 1; i <= 11; i++) {
		var oInputtemp = getControlOfTableRow(toTable, 3, i);
		oInputtemp.setEditable();
	}
	for (i = 1; i <= 11; i++) {
		var oInputtemp = getControlOfTableRow(toTable, 4, i);
		oInputtemp.setVisible(false);
	}

	UpdateCFPACETotal();

	CFPExpectedCasePressed();
}

function UpdateCFPACETotal() {
	var totalACEAmt = getField("CapitalTotal");
	var oInput = getTableRow("CashFlowPaybackTable", 2);
	var amt = -1 * totalACEAmt;
	setTableRowField(oInput, "Year0", amountFormatter(amt));
}

function CFPExpectedCasePressed() {}

function CFPBestCasePressed() {}

function CFPWorstCasePressed() {}


function GetTotalofCFPColumn(colIndex) {
	var ttls = new Array();
	const length = 8;

	for (i = 0; i <= length; i++) {

		var f = -1;

		var CFValue = getTableRowField(getTableRow('CashFlowPaybackTable', i), "Year" + colIndex);

		if (CFValue !== undefined) {
			if (CFValue.indexOf("(") === -1) f = 1;

			var t = parseInt(CFValue.replace(/\,|\(|\)/g, ''));
			ttls[i] = isNaN(t) ? 0 : t * f;
		}
	}
	var sum = ttls.reduce(function(a, b) {
		return a + b;
	}, 0);
	return sum;
}



function BuildCFPData(sc) { //sc = the scenario indicator
	var CFPCheck = getField("CFPCheck");
	if (CFPCheck === false) {
		var cfpTable = "CashFlowPaybackTable";
		var expenseTable = "ExpenseTable" + sc;
		var incomeTable = "IncomeTable" + sc;
		var fromTableModel = "Expenses" + sc;
		var cfpRow0 = getTableRow(cfpTable, 0); //Get 'After Tax Row'
		var cfpRow1 = getTableRow(cfpTable, 1); //Get 'Tax Depreciation Row'
		var cfpRow5 = getTableRow(cfpTable, 5); // Inventory
		var cfpRow6 = getTableRow(cfpTable, 6); // Payables
		var cfpRow7 = getTableRow(cfpTable, 7); // Receivables
		var cfpRow8 = getTableRow(cfpTable, 8); // Residual
		var cfpRow9 = getTableRow(cfpTable, 9); // Annual Net CashFlow
		var cfpRow10 = getTableRow(cfpTable, 10); // Cumulative Net CashFlow
		var expTableLength = getField(fromTableModel).length;
		var incTableLength = getField("Income" + (sc === 'EC' ? '' : sc)).length;
		var row9PreviousVal = 0;
		var cosPreviousVal = 0;
		var trdSlsPreviousVal = 0;
		var totalInvCap = 0;
		var totalRecCap = 0;
		var totalPayCap = 0;
		var workingCapCols = [1, 2, 3, 4, 5];
		var cashflows = [];
		var turns = getField("TurnsFactor");
		var dso = getField("DSOFactor");
		var netdays = getField("NetDaysFactor");
		var paybackPeriod = 0;
		var totalTaxDepr = 0;
		var paybackSet = false;


		for (var col = 0; col <= 10; col++) {
			var taxAddbackTotal = 0;
			var colCOSTotal = 0;
			var colTrdSlsTotal = 0;
			var colPayTotal = 0;

			//After tax net profit
			var taxVal = getFieldAsNumeric("AfterTaxProfit" + sc + "year" + col);
			setTableRowField(cfpRow0, "Year" + col, amountFormatter(taxVal));

			//get values from Expense table
			for (var i = 0; i < expTableLength; i++) {
				var r = getTableRow(expenseTable, i);
				var category = getTableRowField(r, "Category");

				if (category === "Tax Depreciation" || i == 1) {
					var yearTtl = parseIntZero(getTableRowField(r, "Year" + col));
					taxAddbackTotal += yearTtl;
					totalTaxDepr += yearTtl;
				}

				if (category === "Cost of Sales" || i == 0) {
					var rowVal = getTableRowField(r, "Year" + col);
					var cosTtl = parseIntZero(rowVal);
					colCOSTotal += isNaN(cosTtl) ? 0 : cosTtl;
				}
			}

			//get values from Income table
			for (var i = 0; i < incTableLength; i++) {
				var row = getTableRow(incomeTable, i);
				var category = getTableRowField(row, "Category");

				if (category === "Trade Sales" || i == 0) {
					var tsTtl = parseIntZero(getTableRowField(row, "Year" + col));
					colTrdSlsTotal += tsTtl;
				}
			}

			//tax depreciation addback
			setTableRowField(cfpRow1, "Year" + col, amountFormatter(taxAddbackTotal));

			//working capital rows
			if (workingCapCols.indexOf(col) !== -1) {

				if (col === 5) {
					// all the previous 4 columns totaled up * -1;
					setTableRowField(cfpRow5, "Year" + col, amountFormatter(totalInvCap * -1));
					setTableRowField(cfpRow6, "Year" + col, amountFormatter(totalRecCap * -1));
					setTableRowField(cfpRow7, "Year" + col, amountFormatter(totalPayCap * -1));

				} else {

					//inventory
					var inv = Math.round((colCOSTotal - cosPreviousVal) * turns) * -1;
					setTableRowField(cfpRow5, "Year" + col, amountFormatter(inv));
					totalInvCap += inv;

					// Receivables
					var rec = Math.round((colTrdSlsTotal - trdSlsPreviousVal) / dso) * -1
					setTableRowField(cfpRow6, "Year" + col, amountFormatter(rec));
					totalRecCap += rec;

					// Payables
					var pay = Math.round((colCOSTotal - cosPreviousVal) / netdays);
					setTableRowField(cfpRow7, "Year" + col, amountFormatter(pay));
					totalPayCap += pay;
				}

			}
			cosPreviousVal = colCOSTotal;
			trdSlsPreviousVal = colTrdSlsTotal;

			//Annual Net Cashflow
			var columnTotal = GetTotalofCFPColumn(col);
			setTableRowField(cfpRow9, "Year" + col, amountFormatter(columnTotal));
			cashflows.push(columnTotal);

			//Cumulative Cash Flow
			//if year zero, be the same value as year 0, row 9
			var currentColumnTtl = columnTotal + row9PreviousVal;
			setTableRowField(cfpRow10, "Year" + col, amountFormatter(currentColumnTtl));

			if (currentColumnTtl > 0 && row9PreviousVal <= 0 && paybackSet === false) {
				paybackPeriod = (row9PreviousVal * -1) / ((row9PreviousVal * -1) + currentColumnTtl) + (col - 1);
				setField("PaybackPeriod", paybackPeriod.toFixed(1).toString());
				paybackSet = true;
			}
			row9PreviousVal += columnTotal;

			//Residual
			if (col === 10) {
				var totalCapital = getField("CapitalTotal");
				if (isNaN(totalCapital) === true) totalCapital = 0;
				totalTaxDepr = parseIntZero(totalTaxDepr);
				totalACEAmt = totalCapital - totalTaxDepr;
				setTableRowField(cfpRow8, "Year" + col, amountFormatter(totalACEAmt));
			}
		}

		var guess = getField("CashFlowInterest");

		if (paybackSet === false) setField("PaybackPeriod", "0");
		var irrRaw = IRR(cashflows, guess) * 100;
		if (guess > 0) setField("IRR", irrRaw.toFixed(2));
	}
}

function setTurnsFactor(evt) {

	var cspbValue = getField("TurnsValue");
	if (cspbValue <= 0 || cspbValue > 1000) {
		setField("TurnsFactor", 0);
	} else {
		setField("TurnsFactor", 100 / cspbValue / 100);
	}
	BuildCFPData(CFFlag);

}

function setDSOFactor(evt) {

	var cspbValue = getField("DSOValue");
	if (cspbValue <= 0 || cspbValue > 1000) {
		setField("DSOFactor", 0);
	} else {
		setField("DSOFactor", 365 / cspbValue);
	}
	BuildCFPData(CFFlag);
}

function setNetDaysFactor(evt) {
	var cspbValue = getField("NetDaysValue");
	if (cspbValue <= 0 || cspbValue > 1000) {
		setField("NetDaysFactor", 0);
	} else {
		setField("NetDaysFactor", 365 / cspbValue);
	}
	BuildCFPData(CFFlag);
}

function amountFormatter(val) {
	if (val !== null) {
		val = val.toString();
		var pattern = /(-?\d+)(\d{3})/;
		while (pattern.test(val))
			val = val.replace(pattern, "$1,$2");
		//remove commas
		val = replaceAll(val, ',', '');

        // convert and round with the commas
		var rounded = Math.round(parseFloat(val)).toString();

		//now add the commas back for formatting
		rounded = rounded.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		if (parseFloat(rounded) < 0) { //check for negative
			var newVal = rounded.replace(/-(?=\d)/, ""); //remove minus sign
			return '(' + newVal + ')';
		} else {
			return rounded;
		}
	}
}

function replaceAll(string, search, replace) {
  return string.split(search).join(replace);
}

function chkString() { //Exclusive for CashFlowPaybackTable Input fields; else remove last line of code and reuse

	var oRow = getTableRow("CashFlowPaybackTable", 3);
	for (i = 0; i <= 10; i++) {
		var temp = getTableRowField(oRow, "Year" + i);
		if (temp !== undefined && temp > 0) {
			var baseCurr = getField("HeaderCurrency");
			var formatter = new Intl.NumberFormat('en-US', {
				style: 'currency',
				currency: baseCurr,
			});
			temp = formatter.format(temp);
			temp = temp.slice(1);

			var splitValue = temp.split(".");
			setTableRowField(oRow, "Year" + i, splitValue[0])
		}
	}
}


// this is used by cells in income and expense section
function chkStringtry(evt) {

	var cellvalue = evt.getSource().getValue();
	var tempcellvalue = parseIntZero(cellvalue);
	evt.getSource().setValue(amountFormatter(tempcellvalue));

	var r = getCurrentRowIndexOfControl(evt);
	if(r === 3) {
	    updateCFRows();
	}

}
var CFFlag = "EC";

function onCFSegmentedSelect(evt) {

	var SelectedKey = evt.getSource().getSelectedKey();
	CFFlag = SelectedKey;
}

function updateCFRows() {

	if (getField("Utility/DataLoading")) return;

	if (ICFlag === CFFlag) {
		BuildCFPData(ICFlag);
	}
}

function addPlants(evt) {
	var oPayload = {};

	var actionParameters = {
		showBusyDialog: true,
		showSuccessMessage: true,
		successFunction: plantsAvailable
	};

	fabPerformAction("ExtractPlants", oPayload, actionParameters, null);
}

function plantsAvailable(evt) {

}

function onMyFileUploadByKey1Complete(evt) {

	sap.m.MessageToast.show("File Uploaded");

	var oModel = getModel("fab");
	var filters = [];
	oFilter1 = fabFilter("Zinstance", getField("Zinstance"));

	filters.push(oFilter1);
	oFilter2 = fabFilter("Zkey", "leaseFileKey2");
	filters.push(oFilter2);

	oModel.read("/Attachments3", {
		success: function(data) {

			if (data.results) {
				if (data.results.length > 0) {
					setField("LeaseFileURL", data.results[0].UrlContent);
					setField("LeaseFileName", data.results[0].Objdes + "." + data.results[0].FileExt);
					setField("ShowDeleteLeaseFileBtn", true);
					getControl("leaseFileUploader").setEnabled(false);
				}
			}
		},
		filters: filters
	});

}

function DeleteLeaseFile() {

	sap.m.MessageBox.confirm(
		'Delete this file?', {
			actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CANCEL],
			onClose: function(oAction) {
				if (oAction == "OK") {
					DeleteLeaseFileWorking();
					getControl("leaseFileUploader").setValue("");
				}
			},
		}
	);
}

function DeleteLeaseFileWorking() {
	var oModel = getModel("fab");
	var filters = [];
	var key = "leaseFileKey2";
	oFilter1 = fabFilter("Zinstance", getField("Zinstance"));
	filters.push(oFilter1);
	oFilter2 = fabFilter("Zkey", key);
	filters.push(oFilter2);

	oModel.read("/Attachments3", {
		success: function(data) {

			if (data.results) {

				if (data.results.length > 0) {
					//=====
					for (var i = 0; i < data.results.length; i++) {
						var oAttachment = data.results[i];

						oModel.callFunction("/DeleteAttachmentByKey", {
							urlParameters: {
								Zinstance: getField("Zinstance"),
								Zkey: key,
								Foltp: oAttachment.Foltp,
								Folyr: oAttachment.Folyr,
								Folno: oAttachment.Folno,
								Objtp: oAttachment.Objtp,
								Objno: oAttachment.Objno,
								Objyr: oAttachment.Objyr,
								Objdes: oAttachment.Objdes
							},
							success: function(data2) {
								if (data2.DeleteAttachmentByKey.Success) {
									sap.m.MessageToast.show("Files Remove: " + data.results.length);
									setField("LeaseFileURL", '');
									setField("LeaseFileName", '');

									setField("ShowDeleteLeaseFileBtn", false);
									getControl("leaseFileUploader").setEnabled(true);
								} else {
									sap.m.MessageToast.show("Error occurred. Please try again.");
								}
							},
							error: function(error) {
								sap.m.MessageToast.show("Error occurred.  Please try again.");
							},

							batchGroupId: "DA"
						});
					} //for

					//=====
				}
			}
		},
		filters: filters
	});
}

function rebindCRLPlantSelect(evt) {
	debugger;
	var iSelectPosition = 3;
	rebindSelectInTableLine(evt, iSelectPosition);
	evt.getSource().getParent().getCells()[3].setValue("");
}
//CostCentre reseting by changing the Plant in CRL.
function PlantChange(evt) {
	debugger;
	evt.getSource().getParent().getCells()[4].setValue("");
	UpdateProjectTitle(null);
}

function onCFPFileUploadByKey1Complete(evt) {
	debugger;

	sap.m.MessageToast.show("File Uploaded");

	var oModel = getModel("fab");
	var filters = [];
	oFilter1 = fabFilter("Zinstance", getField("Zinstance"));
	filters.push(oFilter1);
	oFilter2 = fabFilter("Zkey", "AttachCFPKey");
	filters.push(oFilter2);

	oModel.read("/Attachments3", {
		success: function(data) {

			if (data.results) {
				if (data.results.length > 0) {
					setField("CFPURL", data.results[0].UrlContent);
					setField("CFPName", data.results[0].Objdes + "." + data.results[0].FileExt);
					setField("ShowDeleteCFPBtn", true);
					getControl("CFPFileUploader").setEnabled(false);
				}
			}
		},
		filters: filters
	});

}

function DeleteCFP() {
	debugger;
	sap.m.MessageBox.confirm(
		'Delete this file?', {
			actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CANCEL],
			onClose: function(oAction) {
				if (oAction == "OK") {
					DeleteCFPFileWorking();
					getControl("CFPFileUploader").setValue("");
				}
			},
		}
	);
}

function DeleteCFPFileWorking() {
	debugger;

	var oModel = getModel("fab");
	var filters = [];
	var key = "AttachCFPKey";
	oFilter1 = fabFilter("Zinstance", getField("Zinstance"));
	filters.push(oFilter1);
	oFilter2 = fabFilter("Zkey", key);
	filters.push(oFilter2);

	oModel.read("/Attachments3", {
		success: function(data) {

			if (data.results) {

				if (data.results.length > 0) {
					//=====
					for (var i = 0; i < data.results.length; i++) {
						var oAttachment = data.results[i];

						oModel.callFunction("/DeleteAttachmentByKey", {
							urlParameters: {
								Zinstance: getField("Zinstance"),
								Zkey: key,
								Foltp: oAttachment.Foltp,
								Folyr: oAttachment.Folyr,
								Folno: oAttachment.Folno,
								Objtp: oAttachment.Objtp,
								Objno: oAttachment.Objno,
								Objyr: oAttachment.Objyr,
								Objdes: oAttachment.Objdes
							},
							success: function(data2) {
								if (data2.DeleteAttachmentByKey.Success) {
									sap.m.MessageToast.show("Files Remove: " + data.results.length);
									setField("CFPURL", '');
									setField("CFPName", '');

									setField("ShowDeleteCFPBtn", false);
									getControl("CFPFileUploader").setEnabled(true);
								} else {
									sap.m.MessageToast.show("Error occurred. Please try again.");
								}
							},
							error: function(error) {
								sap.m.MessageToast.show("Error occurred. Please try again.");
							},
							batchGroupId: "DA"
						});
					} //for

				}
			}
		},
		filters: filters
	});
}

function CRLRemoveColumns(evt) {
	debugger;
	setTimeout(function() {
		CRLCurrencyConversion(evt)
	}, 1000);
}


function uploadconversion(oContents){
    IEImportExcel(oContents);
    // var event = new Event('build');
    CRLCurrencyConversion(null);
}

function IEImportExcel(oContents) {
	debugger;

	setField("Utility/DataLoading", true);


	var ExpectedCaseLength = oContents["Expected Case"].valueOf().length;
	var WorstCaseLength = oContents["Worst Case"].valueOf().length;
	var BestCaseLength = oContents["Best Case"].valueOf().length;
    var CapitalLength = oContents["Capital"].valueOf().length;

    var Capital = [];
	var ExpectedCaseIncome = [];
	var ExpectedCaseDeduction = [];

	var ExpectedCaseOE = [];
	var ExpectedCaseSGA = [];
	var WorstCaseIncome = [];
	var WorstCaseDeduction = [];
	var WorstCaseOE = [];
	var WorstCaseSGA = [];
	var BestCaseIncome = [];
	var BestCaseDeduction = [];
	var BestCaseOE = [];
	var BestCaseSGA = [];
	for (i = 0; i < ExpectedCaseLength; i++) {
		var Category = oContents["Expected Case"].valueOf()[i]["Expected Case"];
		if (Category === "Units") {
			var UnitsEC = oContents["Expected Case"].valueOf()[i]["CARLISLE CONSTRUCTION MATERIALS"];
			setField("UnitsEC", UnitsEC);
		}
	}

	for (i = 0; i < WorstCaseLength; i++) {

		var Category = oContents["Worst Case"].valueOf()[i]["Worst Case"];
		if (Category === "Units") {
			var UnitsWC = oContents["Worst Case"].valueOf()[i]["CARLISLE CONSTRUCTION MATERIALS"];
			setField("UnitsWC", UnitsWC);
		}
	}

	for (i = 0; i < BestCaseLength; i++) {
		var Category = oContents["Best Case"].valueOf()[i]["Best Case"];
		if (Category === "Units") {
			var UnitsBC = oContents["Best Case"].valueOf()[i]["CARLISLE CONSTRUCTION MATERIALS"];
			setField("UnitsBC", UnitsBC);
		}
	}

    var start = false;
    for(i = 0; i < CapitalLength; i++){
    var qty = oContents["Capital"].valueOf()[i]["__EMPTY"];
    if(qty === "Add rows above") break;
        if (start === true) {
            var CRL = {};
            CRL.Capital     = oContents["Capital"].valueOf()[i]["__EMPTY_6"];
            CRL.Expense      = oContents["Capital"].valueOf()[i]["__EMPTY_7"];
    if(CRL.Capital > 0 || CRL.Expense > 0){
            CRL.Quantity = oContents["Capital"].valueOf()[i]["__EMPTY"];
            if (CRL.Quantity === undefined) CRL.Quantity = 1;
            CRL.Description  = oContents["Capital"].valueOf()[i]["__EMPTY_1"];
            CRL.Company      = oContents["Capital"].valueOf()[i]["__EMPTY_2"];
            CRL.Plant        = oContents["Capital"].valueOf()[i]["__EMPTY_3"];
            CRL.CostCenter   = oContents["Capital"].valueOf()[i]["__EMPTY_4"];
            CRL.CurrencyCode = oContents["Capital"].valueOf()[i]["__EMPTY_5"];

            CRL.Total        = CRL.Capital - CRL.Expense;
            Capital.push(CRL);
            }
        }
        if (qty === "Add rows below") start = true;
    }

    if(Capital.length !== 0){
        setField("CapitalRequestList", Capital);
    }

	for (i = 0; i < ExpectedCaseLength; i++) {


		var Category = oContents["Expected Case"].valueOf()[i]["Expected Case"];
		var IncomeRow = oContents["Expected Case"].valueOf()[i]["__rowNum__"];
		if (Category === "Trade Sales" || Category === "Inter Company Sales") {

			var EI = {};

			var j = i;
			EI.Category = oContents["Expected Case"].valueOf()[j]["Expected Case"];
			if (EI["Category"] === "TOTAL GROSS SALES") {
				break;
			}
			EI.Description = oContents["Expected Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			EI.Year0 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Expected Case"].valueOf()[j]["__EMPTY"];
			EI.Year1 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Expected Case"].valueOf()[j]["__EMPTY_1"];
			EI.Year2 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Expected Case"].valueOf()[j]["__EMPTY_2"];
			EI.Year3 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Expected Case"].valueOf()[j]["__EMPTY_3"];
			EI.Year4 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Expected Case"].valueOf()[j]["__EMPTY_4"];
			EI.Year5 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Expected Case"].valueOf()[j]["__EMPTY_5"];
			EI.Year6 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Expected Case"].valueOf()[j]["__EMPTY_6"];
			EI.Year7 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Expected Case"].valueOf()[j]["__EMPTY_7"];
			EI.Year8 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Expected Case"].valueOf()[j]["__EMPTY_8"];
			EI.Year9 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Expected Case"].valueOf()[j]["__EMPTY_9"];
			EI.Year10 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Expected Case"].valueOf()[j]["__EMPTY_10"];
			EI.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			ExpectedCaseIncome.push(EI);

		}
		setField("Income", ExpectedCaseIncome);

	}

	for (i = 0; i < ExpectedCaseLength; i++) {

		var Category = oContents["Expected Case"].valueOf()[i]["Expected Case"];

		if (Category === "Deductions") {

			var ED = {};

			var j = i;
			ED.Category = oContents["Expected Case"].valueOf()[j]["Expected Case"];
			if (ED["Category"] === "NET SALES") {
				break;
			}
			ED.Description = oContents["Expected Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			ED.Year0 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Expected Case"].valueOf()[j]["__EMPTY"];
			ED.Year1 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Expected Case"].valueOf()[j]["__EMPTY_1"];
			ED.Year2 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Expected Case"].valueOf()[j]["__EMPTY_2"];
			ED.Year3 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Expected Case"].valueOf()[j]["__EMPTY_3"];
			ED.Year4 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Expected Case"].valueOf()[j]["__EMPTY_4"];
			ED.Year5 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Expected Case"].valueOf()[j]["__EMPTY_5"];
			ED.Year6 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Expected Case"].valueOf()[j]["__EMPTY_6"];
			ED.Year7 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Expected Case"].valueOf()[j]["__EMPTY_7"];
			ED.Year8 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Expected Case"].valueOf()[j]["__EMPTY_8"];
			ED.Year9 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Expected Case"].valueOf()[j]["__EMPTY_9"];
			ED.Year10 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Expected Case"].valueOf()[j]["__EMPTY_10"];
			ED.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			ExpectedCaseDeduction.push(ED);

		}
		setField("DeductionsEC", ExpectedCaseDeduction);

	}

	for (i = 0; i < ExpectedCaseLength; i++) {

		var Category = oContents["Expected Case"].valueOf()[i]["Expected Case"];

		if (Category === "Direct Material"
		    || Category === "Direct Labor"
		   || Category === "Indirect Labor"
		 || Category === "Benefits"
		    || Category === "Rental"
		    || Category === "Repair & Maintenance"
		    || Category === "Supplies"
		    || Category === "Freight"
		    || Category === "Other Operating Expense"
		    || Category === "Cost of Sales"
		    || Category === "Tax Depreciation"
		    || Category === "Project Expense") {

			var EOE = {};

			var j = i;
			if (Category === "Repair & Maintenance") {
				EOE.Category = "Repair Maintenance";
			} else {

				EOE.Category = oContents["Expected Case"].valueOf()[j]["Expected Case"];
			}
			if (EOE["Category"] === "GROSS MARGIN") {
				break;
			}
			EOE.Description = oContents["Expected Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			EOE.Year0 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Expected Case"].valueOf()[j]["__EMPTY"];
			EOE.Year1 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Expected Case"].valueOf()[j]["__EMPTY_1"];
			EOE.Year2 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Expected Case"].valueOf()[j]["__EMPTY_2"];
			EOE.Year3 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Expected Case"].valueOf()[j]["__EMPTY_3"];
			EOE.Year4 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Expected Case"].valueOf()[j]["__EMPTY_4"];
			EOE.Year5 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Expected Case"].valueOf()[j]["__EMPTY_5"];
			EOE.Year6 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Expected Case"].valueOf()[j]["__EMPTY_6"];
			EOE.Year7 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Expected Case"].valueOf()[j]["__EMPTY_7"];
			EOE.Year8 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Expected Case"].valueOf()[j]["__EMPTY_8"];
			EOE.Year9 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Expected Case"].valueOf()[j]["__EMPTY_9"];
			EOE.Year10 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Expected Case"].valueOf()[j]["__EMPTY_10"];
			EOE.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			ExpectedCaseOE.push(EOE);

		}
		setField("ExpensesEC", ExpectedCaseOE);

	}


	for (i = 0; i < ExpectedCaseLength; i++) {

		var Category = oContents["Expected Case"].valueOf()[i]["Expected Case"];

		if (Category === "Selling Expense" || Category === "Commissions" || Category === "G&A Expense" || Category === "Start-up Expense" || Category === "Other SG&A") {

			var ESGA = {};

			var j = i;
			if (Category === "G&A Expense") {
				ESGA.Category = "GA Expense";
			} else if (Category === "Start-up Expense") {
				ESGA.Category = "Startup Expense";
			} else if (Category === "Other SG&A") {
				ESGA.Category = "Other SGA";

			} else {
				ESGA.Category = oContents["Expected Case"].valueOf()[j]["Expected Case"];
			}
			if (ESGA["Category"] === "PRE-TAX PROFIT") {
				break;
			}
			ESGA.Description = oContents["Expected Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			ESGA.Year0 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Expected Case"].valueOf()[j]["__EMPTY"];
			ESGA.Year1 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Expected Case"].valueOf()[j]["__EMPTY_1"];
			ESGA.Year2 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Expected Case"].valueOf()[j]["__EMPTY_2"];
			ESGA.Year3 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Expected Case"].valueOf()[j]["__EMPTY_3"];
			ESGA.Year4 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Expected Case"].valueOf()[j]["__EMPTY_4"];
			ESGA.Year5 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Expected Case"].valueOf()[j]["__EMPTY_5"];
			ESGA.Year6 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Expected Case"].valueOf()[j]["__EMPTY_6"];
			ESGA.Year7 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Expected Case"].valueOf()[j]["__EMPTY_7"];
			ESGA.Year8 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Expected Case"].valueOf()[j]["__EMPTY_8"];
			ESGA.Year9 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Expected Case"].valueOf()[j]["__EMPTY_9"];
			ESGA.Year10 = amountFormatter(oContents["Expected Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Expected Case"].valueOf()[j]["__EMPTY_10"];
			ESGA.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			ExpectedCaseSGA.push(ESGA);

		}
		setField("OtherExpensesEC", ExpectedCaseSGA);

	}

	for (i = 0; i < WorstCaseLength; i++) {

		var Category = oContents["Worst Case"].valueOf()[i]["Worst Case"];
		if (Category === "Trade Sales" || Category === "Inter Company Sales") {

			var WI = {};

			var j = i;
			WI.Category = oContents["Worst Case"].valueOf()[j]["Worst Case"];
			if (WI["Category"] === "TOTAL GROSS SALES") {
				break;
			}
			WI.Description = oContents["Worst Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			WI.Year0 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Worst Case"].valueOf()[j]["__EMPTY"];
			WI.Year1 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Worst Case"].valueOf()[j]["__EMPTY_1"];
			WI.Year2 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Worst Case"].valueOf()[j]["__EMPTY_2"];
			WI.Year3 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Worst Case"].valueOf()[j]["__EMPTY_3"];
			WI.Year4 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Worst Case"].valueOf()[j]["__EMPTY_4"];
			WI.Year5 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Worst Case"].valueOf()[j]["__EMPTY_5"];
			WI.Year6 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Worst Case"].valueOf()[j]["__EMPTY_6"];
			WI.Year7 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Worst Case"].valueOf()[j]["__EMPTY_7"];
			WI.Year8 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Worst Case"].valueOf()[j]["__EMPTY_8"];
			WI.Year9 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Worst Case"].valueOf()[j]["__EMPTY_9"];
			WI.Year10 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Worst Case"].valueOf()[j]["__EMPTY_10"];
			WI.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			WorstCaseIncome.push(WI);

		}
		setField("IncomeWC", WorstCaseIncome);

	}

	for (i = 0; i < WorstCaseLength; i++) {

		var Category = oContents["Worst Case"].valueOf()[i]["Worst Case"];

		if (Category === "Deductions") {

			var WD = {};

			var j = i;
			WD.Category = oContents["Worst Case"].valueOf()[j]["Worst Case"];
			if (WD["Category"] === "NET SALES") {
				break;
			}
			WD.Description = oContents["Worst Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			WD.Year0 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Worst Case"].valueOf()[j]["__EMPTY"];
			WD.Year1 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Worst Case"].valueOf()[j]["__EMPTY_1"];
			WD.Year2 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Worst Case"].valueOf()[j]["__EMPTY_2"];
			WD.Year3 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Worst Case"].valueOf()[j]["__EMPTY_3"];
			WD.Year4 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Worst Case"].valueOf()[j]["__EMPTY_4"];
			WD.Year5 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Worst Case"].valueOf()[j]["__EMPTY_5"];
			WD.Year6 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Worst Case"].valueOf()[j]["__EMPTY_6"];
			WD.Year7 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Worst Case"].valueOf()[j]["__EMPTY_7"];
			WD.Year8 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Worst Case"].valueOf()[j]["__EMPTY_8"];
			WD.Year9 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Worst Case"].valueOf()[j]["__EMPTY_9"];
			WD.Year10 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Worst Case"].valueOf()[j]["__EMPTY_10"];
			WD.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			WorstCaseDeduction.push(WD);

		}
		setField("DeductionsWC", WorstCaseDeduction);

	}

	for (i = 0; i < WorstCaseLength; i++) {

		var Category = oContents["Worst Case"].valueOf()[i]["Worst Case"];

		if (Category === "Direct Material"
		    || Category === "Direct Labor"
		    || Category === "Indirect Labor"
		    || Category === "Benefits"
		    || Category === "Rental"
		    || Category === "Repair & Maintenance"
		    || Category === "Freight"
		    || Category === "Supplies"
		  || Category === "Other Operating Expense"
		    || Category === "Cost of Sales"
		    || Category === "Tax Depreciation"
		    || Category === "Project Expense") {

			var WOE = {};

			var j = i;
			if (Category === "Repair & Maintenance") {
				WOE.Category = "Repair Maintenance";
			} else {

				WOE.Category = oContents["Worst Case"].valueOf()[j]["Worst Case"];
			}
			if (WOE["Category"] === "GROSS MARGIN") {
				break;
			}
			WOE.Description = oContents["Worst Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			WOE.Year0 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Worst Case"].valueOf()[j]["__EMPTY"];
			WOE.Year1 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Worst Case"].valueOf()[j]["__EMPTY_1"];
			WOE.Year2 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Worst Case"].valueOf()[j]["__EMPTY_2"];
			WOE.Year3 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Worst Case"].valueOf()[j]["__EMPTY_3"];
			WOE.Year4 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Worst Case"].valueOf()[j]["__EMPTY_4"];
			WOE.Year5 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Worst Case"].valueOf()[j]["__EMPTY_5"];
			WOE.Year6 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Worst Case"].valueOf()[j]["__EMPTY_6"];
			WOE.Year7 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Worst Case"].valueOf()[j]["__EMPTY_7"];
			WOE.Year8 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Worst Case"].valueOf()[j]["__EMPTY_8"];
			WOE.Year9 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Worst Case"].valueOf()[j]["__EMPTY_9"];
			WOE.Year10 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Worst Case"].valueOf()[j]["__EMPTY_10"];
			WOE.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			WorstCaseOE.push(WOE);

		}
		setField("ExpensesWC", WorstCaseOE);

	}

	for (i = 0; i < WorstCaseLength; i++) {

		var Category = oContents["Worst Case"].valueOf()[i]["Worst Case"];

		if (Category === "Selling Expense" || Category === "Commissions" || Category === "G&A Expense" || Category === "Start-up Expense" || Category === "Other SG&A") {

			var WSGA = {};

			var j = i;
			if (Category === "G&A Expense") {
				WSGA.Category = "GA Expense";
			} else if (Category === "Start-up Expense") {
				WSGA.Category = "Startup Expense";
			} else if (Category === "Other SG&A") {
				WSGA.Category = "Other SGA";
			} else {
				WSGA.Category = oContents["Worst Case"].valueOf()[j]["Worst Case"];
			}
			if (WSGA["Category"] === "PRE-TAX PROFIT") {
				break;
			}
			WSGA.Description = oContents["Worst Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			WSGA.Year0 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Worst Case"].valueOf()[j]["__EMPTY"];
			WSGA.Year1 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Worst Case"].valueOf()[j]["__EMPTY_1"];
			WSGA.Year2 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Worst Case"].valueOf()[j]["__EMPTY_2"];
			WSGA.Year3 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Worst Case"].valueOf()[j]["__EMPTY_3"];
			WSGA.Year4 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Worst Case"].valueOf()[j]["__EMPTY_4"];
			WSGA.Year5 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Worst Case"].valueOf()[j]["__EMPTY_5"];
			WSGA.Year6 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Worst Case"].valueOf()[j]["__EMPTY_6"];
			WSGA.Year7 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Worst Case"].valueOf()[j]["__EMPTY_7"];
			WSGA.Year8 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Worst Case"].valueOf()[j]["__EMPTY_8"];
			WSGA.Year9 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Worst Case"].valueOf()[j]["__EMPTY_9"];
			WSGA.Year10 = amountFormatter(oContents["Worst Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Worst Case"].valueOf()[j]["__EMPTY_10"];
			WSGA.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			WorstCaseSGA.push(WSGA);

		}
		setField("OtherExpensesWC", WorstCaseSGA);

	}

	for (i = 0; i < BestCaseLength; i++) {

		var Category = oContents["Best Case"].valueOf()[i]["Best Case"];
		if (Category === "Trade Sales" || Category === "Inter Company Sales") {

			var BI = {};

			var j = i;
			BI.Category = oContents["Best Case"].valueOf()[j]["Best Case"];
			if (BI["Category"] === "TOTAL GROSS SALES") {
				break;
			}
			BI.Description = oContents["Best Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			BI.Year0 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Best Case"].valueOf()[j]["__EMPTY"];
			BI.Year1 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Best Case"].valueOf()[j]["__EMPTY_1"];
			BI.Year2 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Best Case"].valueOf()[j]["__EMPTY_2"];
			BI.Year3 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Best Case"].valueOf()[j]["__EMPTY_3"];
			BI.Year4 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Best Case"].valueOf()[j]["__EMPTY_4"];
			BI.Year5 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Best Case"].valueOf()[j]["__EMPTY_5"];
			BI.Year6 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Best Case"].valueOf()[j]["__EMPTY_6"];
			BI.Year7 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Best Case"].valueOf()[j]["__EMPTY_7"];
			BI.Year8 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Best Case"].valueOf()[j]["__EMPTY_8"];
			BI.Year9 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Best Case"].valueOf()[j]["__EMPTY_9"];
			BI.Year10 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Best Case"].valueOf()[j]["__EMPTY_10"];
			BI.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			BestCaseIncome.push(BI);

		}
		setField("IncomeBC", BestCaseIncome);

	}

	for (i = 0; i < BestCaseLength; i++) {

		var Category = oContents["Best Case"].valueOf()[i]["Best Case"];

		if (Category === "Deductions") {

			var BD = {};

			var j = i;
			BD.Category = oContents["Best Case"].valueOf()[j]["Best Case"];
			if (BD["Category"] === "NET SALES") {
				break;
			}
			BD.Description = oContents["Best Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			BD.Year0 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Best Case"].valueOf()[j]["__EMPTY"];
			BD.Year1 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Best Case"].valueOf()[j]["__EMPTY_1"];
			BD.Year2 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Best Case"].valueOf()[j]["__EMPTY_2"];
			BD.Year3 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Best Case"].valueOf()[j]["__EMPTY_3"];
			BD.Year4 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Best Case"].valueOf()[j]["__EMPTY_4"];
			BD.Year5 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Best Case"].valueOf()[j]["__EMPTY_5"];
			BD.Year6 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Best Case"].valueOf()[j]["__EMPTY_6"];
			BD.Year7 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Best Case"].valueOf()[j]["__EMPTY_7"];
			BD.Year8 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Best Case"].valueOf()[j]["__EMPTY_8"];
			BD.Year9 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Best Case"].valueOf()[j]["__EMPTY_9"];
			BD.Year10 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Best Case"].valueOf()[j]["__EMPTY_10"];
			BD.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			BestCaseDeduction.push(BD);

		}
		setField("DeductionsBC", BestCaseDeduction);

	}

	for (i = 0; i < BestCaseLength; i++) {

		var Category = oContents["Best Case"].valueOf()[i]["Best Case"];

		if (Category === "Direct Material"
		|| Category === "Direct Labor"
		|| Category === "Indirect Labor"
		|| Category === "Benefits"
		|| Category === "Rental"
		|| Category === "Repair & Maintenance"
		|| Category === "Freight"
		|| Category === "Supplies"
		|| Category === "Other Operating Expense"
		|| Category === "Cost of Sales"
		|| Category === "Tax Depreciation"
		|| Category === "Project Expense") {

			var BOE = {};

			var j = i;
			if (Category === "Repair & Maintenance") {
				BOE.Category = "Repair Maintenance";
			} else {

				BOE.Category = oContents["Best Case"].valueOf()[j]["Best Case"];
			}

			if (BOE["Category"] === "GROSS MARGIN") {
				break;
			}
			BOE.Description = oContents["Best Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			BOE.Year0 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Best Case"].valueOf()[j]["__EMPTY"];
			BOE.Year1 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Best Case"].valueOf()[j]["__EMPTY_1"];
			BOE.Year2 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Best Case"].valueOf()[j]["__EMPTY_2"];
			BOE.Year3 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Best Case"].valueOf()[j]["__EMPTY_3"];
			BOE.Year4 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Best Case"].valueOf()[j]["__EMPTY_4"];
			BOE.Year5 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Best Case"].valueOf()[j]["__EMPTY_5"];
			BOE.Year6 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Best Case"].valueOf()[j]["__EMPTY_6"];
			BOE.Year7 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Best Case"].valueOf()[j]["__EMPTY_7"];
			BOE.Year8 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Best Case"].valueOf()[j]["__EMPTY_8"];
			BOE.Year9 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Best Case"].valueOf()[j]["__EMPTY_9"];
			BOE.Year10 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Best Case"].valueOf()[j]["__EMPTY_10"];
			BOE.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			BestCaseOE.push(BOE);

		}
		setField("ExpensesBC", BestCaseOE);

	}

	for (i = 0; i < BestCaseLength; i++) {

		var Category = oContents["Best Case"].valueOf()[i]["Best Case"];


		if (Category === "Selling Expense" || Category === "Commissions" || Category === "G&A Expense" || Category === "Start-up Expense" || Category === "Other SG&A") {

			var BSGA = {};

			var j = i;
			if (Category === "G&A Expense") {
				BSGA.Category = "GA Expense";
			} else if (Category === "Start-up Expense") {
				BSGA.Category = "Startup Expense";
			} else if (Category === "Other SG&A") {
				BSGA.Category = "Other SGA";
			} else {
				BSGA.Category = oContents["Best Case"].valueOf()[j]["Best Case"];
			}
			if (BSGA["Category"] === "PRE-TAX PROFIT") {
				break;
			}
			BSGA.Description = oContents["Best Case"].valueOf()[j]["CARLISLE CONSTRUCTION MATERIALS"];
			BSGA.Year0 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY"]);
			year0 = oContents["Best Case"].valueOf()[j]["__EMPTY"];
			BSGA.Year1 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_1"]);
			year1 = oContents["Best Case"].valueOf()[j]["__EMPTY_1"];
			BSGA.Year2 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_2"]);
			year2 = oContents["Best Case"].valueOf()[j]["__EMPTY_2"];
			BSGA.Year3 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_3"]);
			year3 = oContents["Best Case"].valueOf()[j]["__EMPTY_3"];
			BSGA.Year4 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_4"]);
			year4 = oContents["Best Case"].valueOf()[j]["__EMPTY_4"];
			BSGA.Year5 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_5"]);
			year5 = oContents["Best Case"].valueOf()[j]["__EMPTY_5"];
			BSGA.Year6 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_6"]);
			year6 = oContents["Best Case"].valueOf()[j]["__EMPTY_6"];
			BSGA.Year7 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_7"]);
			year7 = oContents["Best Case"].valueOf()[j]["__EMPTY_7"];
			BSGA.Year8 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_8"]);
			year8 = oContents["Best Case"].valueOf()[j]["__EMPTY_8"];
			BSGA.Year9 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_9"]);
			year9 = oContents["Best Case"].valueOf()[j]["__EMPTY_9"];
			BSGA.Year10 = amountFormatter(oContents["Best Case"].valueOf()[j]["__EMPTY_10"]);
			year10 = oContents["Best Case"].valueOf()[j]["__EMPTY_10"];
			BSGA.Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 +year9 + year10);
			BestCaseSGA.push(BSGA);

		}
		setField("OtherExpensesBC", BestCaseSGA);

	}


	getControl("IncomeExpenseExcelDropDialog").close();

	setTimeout(function() {
		setImportPanels();
		// to stop bug where expense rows are not disabled on first import
		ExpenseECRefreshed();
		ExpenseWCRefreshed();
		ExpenseBCRefreshed();
	}, 3000);
}

function setImportPanels() {
    setScenario("EC");
}

function RemoveRowTotalCalculate(evt) {
	debugger;
	IncomeECColumnTotal();
	IncomeWCColumnTotal();
	IncomeBCColumnTotal();
	DeductionsECColumnTotal();
	DeductionsWCColumnTotal();
	DeductionsBCColumnTotal();
	ExpenseECColumnTotal();
	ExpenseWCColumnTotal();
	ExpenseBCColumnTotal();
	OtherExpenseECColumnTotal();
	OtherExpenseWCColumnTotal();
	OtherExpenseBCColumnTotal();
}

function setTemplateUrl() {
	debugger;
	var hostUrl = window.location.href;
	var UrlSplit = hostUrl.split("/");
	var templateUrl = UrlSplit[0] + "//" + UrlSplit[2] + "/sap/bc/bsp/sap/ZCARLISLECAPEX/ACE_IE_Template.xlsx";
	// 	setField("TemplateURL", templateUrl);
	window.open(templateUrl, "_blank");
}

function CreateInternalOrder(evt) {
    debugger;

	var createOrder = getField("SapOrderSwitch");
	var AceNumber = getField("DocumentNumber");
	AceNumber = AceNumber.replace("-", "00");
	setField("InternalOrderNumber", AceNumber);

	if (createOrder === true) {
		var payload = {};

		var actionParameters = {
			showBusyDialog: true,
			busyDialogMessage: "processing..",
			busyDialogTitle: "Please wait",
			showSuccessMessage: true,
			successFunction: successBAPI,
			errorFunction: null
		};

		fabPerformAction("SUBMIT", payload, actionParameters, null);
	}
}
function successBAPI(evt, results) {
    debugger;
    //retrieve the result payload which was passed back as JSON string
    if (results.length === 1) {
        var result = JSON.parse(results[0]);
        fabAlert(result);
 }}


//MultiSelectInput functionality to select multiple companies in RequestInfo Section
function handleValueHelp(evt) {

	var sInputValue = evt.getSource().getValue();
	getControl("CompaniesDialog").open();
	getControl("CompaniesDialog").fireSearch();
	setField("Companies", []);

	var oPayload = {};
	var actionParameters = {
		showBusyDialog: true,
		showSuccessMessage: true,
		successFunction: CompanySelectSuccess
	};

	fabPerformAction("Companies", oPayload, actionParameters, null);

}

function success() {
	lockCRL();
}

function lockCRL() {
	if (getField("SelectedCompanies").length === 0) {
		setField("HasCompanies", false);
	} else setField("HasCompanies", true);
}

function CompanySelectSuccess() {

}

function SetRejectedAction(evt) {
    setField("TaskActioned/AccountingManager", true);
    return true;
}

//Search Functionality in Companies search help on search dialog
function handleValueHelpSearch(evt) {
	var sValue = evt.getParameter("value");

	if (sValue === undefined) {
		sValue = "";
	}
	var Validation = isNaN(sValue);

	if (Validation === false) {
		var oFilter = new sap.ui.model.Filter(
			"companycode",
			sap.ui.model.FilterOperator.Contains,
			sValue
		);
		evt.getSource().getBinding("items").filter([oFilter]);
	} else {
		var oFilter1 = new sap.ui.model.Filter(
			"company",
			sap.ui.model.FilterOperator.Contains,
			sValue
		);
		evt.getSource().getBinding("items").filter([oFilter1]);
	}
}

//Add and close Functionality in Companies search help on search dialog
function handleValueHelpClose(evt) {

	var aSelectedItems = evt.getParameter("selectedItems"),
		oMultiInput = getControl("RequestInfomultiInput");

	if (aSelectedItems && aSelectedItems.length > 0) {
		aSelectedItems.forEach(function(oItem) {
			oMultiInput.addToken(new sap.m.Token({
				text: oItem.getDescription()
			}));
		});
	}
	BeforeSave(evt);}

	function IRR(values, guess) {
  // Credits: algorithm inspired by Apache OpenOffice

  // Calculates the resulting amount
  var irrResult = function(values, dates, rate) {
    var r = rate + 1;
    var result = values[0];
   for (var i = 1; i < values.length; i++) {
      result += values[i] / Math.pow(r, (dates[i] - dates[0]) / 365);
    }
    return result;
  }

  // Calculates the first derivation
  var irrResultDeriv = function(values, dates, rate) {
    var r = rate + 1;
    var result = 0;
    for (var i = 1; i < values.length; i++) {
      var frac = (dates[i] - dates[0]) / 365;
      result -= frac * values[i] / Math.pow(r, frac + 1);
    }
    return result;
  }

  // Initialize dates and check that values contains at least one positive value and one negative value
  var dates = [];
  var positive = false;
  var negative = false;
  for (var i = 0; i < values.length; i++) {
    dates[i] = (i === 0) ? 0 : dates[i - 1] + 365;
    if (values[i] > 0) positive = true;
    if (values[i] < 0) negative = true;
  }

  // Return error if values does not contain at least one positive value and one negative value
  if (!positive || !negative) return '#NUM!';

  // Initialize guess and resultRate
  var guess = (typeof guess === 'undefined') ? 0.1 : guess;
  var resultRate = guess;

  // Set maximum epsilon for end of iteration
  var epsMax = 1e-10;

  // Set maximum number of iterations
  var iterMax = 50;

  // Implement Newton's method
  var newRate, epsRate, resultValue;
  var iteration = 0;
  var contLoop = true;
  do {
    resultValue = irrResult(values, dates, resultRate);
    newRate = resultRate - resultValue / irrResultDeriv(values, dates, resultRate);
    epsRate = Math.abs(newRate - resultRate);
    resultRate = newRate;
    contLoop = (epsRate > epsMax) && (Math.abs(resultValue) > epsMax);
  } while(contLoop && (++iteration < iterMax));

  if(contLoop) return '#NUM!';

  // Return internal rate of return
  return resultRate;}


function IncomeColumnTotal(sc) {
	var Totalyear0 = 0;
	var Totalyear1 = 0;
	var Totalyear2 = 0;
	var Totalyear3 = 0;
	var Totalyear4 = 0;
	var Totalyear5 = 0;
	var Totalyear6 = 0;
	var Totalyear7 = 0;
	var Totalyear8 = 0;
	var Totalyear9 = 0;
	var Totalyear10 = 0;
	var Totaltotal = 0;
	var dataModelSC = "Income" + (sc === 'EC' ? '' : sc)

	var TableLength = getField(dataModelSC).length;
	for (i = 0; i < TableLength; i++) {
		var y0 = getField(dataModelSC)[i].Year0;
		var Y0 = y0.indexOf("(");
		if (Y0 == 0) {
			var year0 = getField(dataModelSC)[i].Year0;
			year0 = year0.replace(/[()]/g, "");
			year0 = "-" + year0;
			year0 = parseIntZero(year0);
		} else {
			var year0 = parseIntZero(getField(dataModelSC)[i].Year0);
		}
		Totalyear0 += year0;
	}
	for (i = 0; i < TableLength; i++) {
		var y1 = getField(dataModelSC)[i].Year1;
		var Y1 = y1.indexOf("(");
		if (Y1 == 0) {
			var year1 = getField(dataModelSC)[i].Year1;
			year1 = year1.replace(/[()]/g, "");
			year1 = "-" + year1;
			year1 = parseIntZero(year1);
		} else {
			var year1 = parseIntZero(getField(dataModelSC)[i].Year1);
		}
		Totalyear1 += year1;
	}
	for (i = 0; i < TableLength; i++) {
		var y2 = getField(dataModelSC)[i].Year2;
		var Y2 = y2.indexOf("(")

		if (Y2 == 0) {
			var year2 = getField(dataModelSC)[i].Year2;
			year2 = year2.replace(/[()]/g, "");
			year2 = "-" + year2;
			year2 = parseIntZero(year2);
		} else {
			var year2 = parseIntZero(getField(dataModelSC)[i].Year2);
		}
		Totalyear2 += year2;
	}
	for (i = 0; i < TableLength; i++) {
		var y3 = getField(dataModelSC)[i].Year3;
		var Y3 = y3.indexOf("(")

		if (Y3 == 0) {
			var year3 = getField(dataModelSC)[i].Year3;
			year3 = year3.replace(/[()]/g, "");
			year3 = "-" + year3;
			year3 = parseIntZero(year3);
		} else {
			var year3 = parseIntZero(getField(dataModelSC)[i].Year3);
		}
		Totalyear3 += year3;
	}
	for (i = 0; i < TableLength; i++) {
		var y4 = getField(dataModelSC)[i].Year4;
		var Y4 = y4.indexOf("(")

		if (Y4 == 0) {
			var year4 = getField(dataModelSC)[i].Year4;
			year4 = year4.replace(/[()]/g, "");
			year4 = "-" + year4;
			year4 = parseIntZero(year4);
		} else {
			var year4 = parseIntZero(getField(dataModelSC)[i].Year4);
		}
		Totalyear4 += year4;
	}
	for (i = 0; i < TableLength; i++) {
		var y5 = getField(dataModelSC)[i].Year5;
		var Y5 = y5.indexOf("(")

		if (Y5 == 0) {
			var year5 = getField(dataModelSC)[i].Year5;
			year5 = year5.replace(/[()]/g, "");
			year5 = "-" + year5;
			year5 = parseIntZero(year5);
		} else {
			var year5 = parseIntZero(getField(dataModelSC)[i].Year5);
		}
		Totalyear5 += year5;
	}
	for (i = 0; i < TableLength; i++) {
		var y6 = getField(dataModelSC)[i].Year6;
		var Y6 = y6.indexOf("(")

		if (Y6 == 0) {
			var year6 = getField(dataModelSC)[i].Year6;
			year6 = year6.replace(/[()]/g, "");
			year6 = "-" + year6;
			year6 = parseIntZero(year6);
		} else {
			var year6 = parseIntZero(getField(dataModelSC)[i].Year6);
		}
		Totalyear6 += year6;
	}
	for (i = 0; i < TableLength; i++) {
		var y7 = getField(dataModelSC)[i].Year7;
		var Y7 = y7.indexOf("(")

		if (Y7 == 0) {
			var year7 = getField(dataModelSC)[i].Year7;
			year7 = year7.replace(/[()]/g, "");
			year7 = "-" + year7;
			year7 = parseIntZero(year7);
		} else {
			var year7 = parseIntZero(getField(dataModelSC)[i].Year7);
		}
		Totalyear7 += year7;
	}
	for (i = 0; i < TableLength; i++) {
		var y8 = getField(dataModelSC)[i].Year8;
		var Y8 = y8.indexOf("(")

		if (Y8 == 0) {
			var year8 = getField(dataModelSC)[i].Year8;
			year8 = year8.replace(/[()]/g, "");
			year8 = "-" + year8;
			year8 = parseIntZero(year8);
		} else {
			var year8 = parseIntZero(getField(dataModelSC)[i].Year8);
		}
		Totalyear8 += year8;
	}
	for (i = 0; i < TableLength; i++) {
		var y9 = getField(dataModelSC)[i].Year9;
		var Y9 = y9.indexOf("(")

		if (Y9 == 0) {
			var year9 = getField(dataModelSC)[i].Year9;
			year9 = year9.replace(/[()]/g, "");
			year9 = "-" + year9;
			year9 = parseIntZero(year9);
		} else {
			var year9 = parseIntZero(getField(dataModelSC)[i].Year9);
		}
		Totalyear9 += year9;
	}
	for (i = 0; i < TableLength; i++) {
		var y10 = getField(dataModelSC)[i].Year10;
		var Y10 = y10.indexOf("(")

		if (Y10 == 0) {
			var year10 = getField(dataModelSC)[i].Year10;
			year10 = year10.replace(/[()]/g, "");
			year10 = "-" + year10;
			year10 = parseIntZero(year10);
		} else {
			var year10 = parseIntZero(getField(dataModelSC)[i].Year10);
		}
		Totalyear10 += year10;
	}
	for (i = 0; i < TableLength; i++) {
		var t = getField(dataModelSC)[i].Total;
		var T = t.indexOf("(")

		if (T == 0) {
			var Totals = getField(dataModelSC)[i].Total;
			Totals = Totals.replace(/[()]/g, "");
			Totals = "-" + Totals;
			Totals = parseIntZero(Totals);
		} else {
			var Totals = parseIntZero(getField(dataModelSC)[i].Total);
		}
		Totaltotal += Totals;
	}
	setField(sc + "year0", amountFormatter(Totalyear0));
	setField(sc + "year1", amountFormatter(Totalyear1));
	setField(sc + "year2", amountFormatter(Totalyear2));
	setField(sc + "year3", amountFormatter(Totalyear3));
	setField(sc + "year4", amountFormatter(Totalyear4));
	setField(sc + "year5", amountFormatter(Totalyear5));
	setField(sc + "year6", amountFormatter(Totalyear6));
	setField(sc + "year7", amountFormatter(Totalyear7));
	setField(sc + "year8", amountFormatter(Totalyear8));
	setField(sc + "year9", amountFormatter(Totalyear9));
	setField(sc + "year10", amountFormatter(Totalyear10));
	setField(sc + "Total", amountFormatter(Totaltotal));

	//Net Sales
	CalculateNetSales(sc);

	//Gross Margin
	CalculateGrossMargin(sc);

	//Pre Tax Profit
	CalculatePreTaxProfit(sc);

	//AfterTaxProfit
	CalculateAfterTaxProfit(sc);


	updateCFRows();
}

function DeductionsColumnTotal(sc) {

	// Called when deductions section is changed.
	var TableLength = getField("Deductions" + sc).length;
	var Totalyear0 = 0;
	var Totalyear1 = 0;
	var Totalyear2 = 0;
	var Totalyear3 = 0;
	var Totalyear4 = 0;
	var Totalyear5 = 0;
	var Totalyear6 = 0;
	var Totalyear7 = 0;
	var Totalyear8 = 0;
	var Totalyear9 = 0;
	var Totalyear10 = 0;
	var Totaltotal = 0;

	for (i = 0; i < TableLength; i++) {
		var y0 = getField("Deductions" + sc)[i].Year0;
		var Y0 = y0.indexOf("(");
		if (Y0 == 0) {
			var year0 = getField("Deductions" + sc)[i].Year0;
			year0 = year0.replace(/[()]/g, "");
			year0 = "-" + year0;
			year0 = parseIntZero(year0);
		} else {
			var year0 = parseIntZero(getField("Deductions" + sc)[i].Year0);
		}
		Totalyear0 += year0;
	}
	for (i = 0; i < TableLength; i++) {
		var y1 = getField("Deductions" + sc)[i].Year1;
		var Y1 = y1.indexOf("(");
		if (Y1 == 0) {
			var year1 = getField("Deductions" + sc)[i].Year1;
			year1 = year1.replace(/[()]/g, "");
			year1 = "-" + year1;
			year1 = parseIntZero(year1);
		} else {
			var year1 = parseIntZero(getField("Deductions" + sc)[i].Year1);
		}
		Totalyear1 += year1;
	}
	for (i = 0; i < TableLength; i++) {
		var y2 = getField("Deductions" + sc)[i].Year2;
		var Y2 = y2.indexOf("(")

		if (Y2 == 0) {
			var year2 = getField("Deductions" + sc)[i].Year2;
			year2 = year2.replace(/[()]/g, "");
			year2 = "-" + year2;
			year2 = parseIntZero(year2);
		} else {
			var year2 = parseIntZero(getField("Deductions" + sc)[i].Year2);
		}
		Totalyear2 += year2;
	}
	for (i = 0; i < TableLength; i++) {
		var y3 = getField("Deductions" + sc)[i].Year3;
		var Y3 = y3.indexOf("(")

		if (Y3 == 0) {
			var year3 = getField("Deductions" + sc)[i].Year3;
			year3 = year3.replace(/[()]/g, "");
			year3 = "-" + year3;
			year3 = parseIntZero(year3);
		} else {
			var year3 = parseIntZero(getField("Deductions" + sc)[i].Year3);
		}
		Totalyear3 += year3;
	}
	for (i = 0; i < TableLength; i++) {
		var y4 = getField("Deductions" + sc)[i].Year4;
		var Y4 = y4.indexOf("(")

		if (Y4 == 0) {
			var year4 = getField("Deductions" + sc)[i].Year4;
			year4 = year4.replace(/[()]/g, "");
			year4 = "-" + year4;
			year4 = parseIntZero(year4);
		} else {
			var year4 = parseIntZero(getField("Deductions" + sc)[i].Year4);
		}
		Totalyear4 += year4;
	}
	for (i = 0; i < TableLength; i++) {
		var y5 = getField("Deductions" + sc)[i].Year5;
		var Y5 = y5.indexOf("(")

		if (Y5 == 0) {
			var year5 = getField("Deductions" + sc)[i].Year5;
			year5 = year5.replace(/[()]/g, "");
			year5 = "-" + year5;
			year5 = parseIntZero(year5);
		} else {
			var year5 = parseIntZero(getField("Deductions" + sc)[i].Year5);
		}
		Totalyear5 += year5;
	}
	for (i = 0; i < TableLength; i++) {
		var y6 = getField("Deductions" + sc)[i].Year6;
		var Y6 = y6.indexOf("(")

		if (Y6 == 0) {
			var year6 = getField("Deductions" + sc)[i].Year6;
			year6 = year6.replace(/[()]/g, "");
			year6 = "-" + year6;
			year6 = parseIntZero(year6);
		} else {
			var year6 = parseIntZero(getField("Deductions" + sc)[i].Year6);
		}
		Totalyear6 += year6;
	}
	for (i = 0; i < TableLength; i++) {
		var y7 = getField("Deductions" + sc)[i].Year7;
		var Y7 = y7.indexOf("(")

		if (Y7 == 0) {
			var year7 = getField("Deductions" + sc)[i].Year7;
			year7 = year7.replace(/[()]/g, "");
			year7 = "-" + year7;
			year7 = parseIntZero(year7);
		} else {
			var year7 = parseIntZero(getField("Deductions" + sc)[i].Year7);
		}
		Totalyear7 += year7;
	}
	for (i = 0; i < TableLength; i++) {
		var y8 = getField("Deductions" + sc)[i].Year8;
		var Y8 = y8.indexOf("(")

		if (Y8 == 0) {
			var year8 = getField("Deductions" + sc)[i].Year8;
			year8 = year8.replace(/[()]/g, "");
			year8 = "-" + year8;
			year8 = parseIntZero(year8);
		} else {
			var year8 = parseIntZero(getField("Deductions" + sc)[i].Year8);
		}
		Totalyear8 += year8;
	}
	for (i = 0; i < TableLength; i++) {
		var y9 = getField("Deductions" + sc)[i].Year9;
		var Y9 = y9.indexOf("(")

		if (Y9 == 0) {
			var year9 = getField("Deductions" + sc)[i].Year9;
			year9 = year9.replace(/[()]/g, "");
			year9 = "-" + year9;
			year9 = parseIntZero(year9);
		} else {
			var year9 = parseIntZero(getField("Deductions" + sc)[i].Year9);
		}
		Totalyear9 += year9;
	}
	for (i = 0; i < TableLength; i++) {
		var y10 = getField("Deductions" + sc)[i].Year10;
		var Y10 = y10.indexOf("(")

		if (Y10 == 0) {
			var year10 = getField("Deductions" + sc)[i].Year10;
			year10 = year10.replace(/[()]/g, "");
			year10 = "-" + year10;
			year10 = parseIntZero(year10);
		} else {
			var year10 = parseIntZero(getField("Deductions" + sc)[i].Year10);
		}
		Totalyear10 += year10;
	}
	for (i = 0; i < TableLength; i++) {
		var t = getField("Deductions" + sc)[i].Total;
		var T = t.indexOf("(")

		if (T == 0) {
			var Totals = getField("Deductions" + sc)[i].Total;
			Totals = Totals.replace(/[()]/g, "");
			Totals = "-" + Totals;
			Totals = parseIntZero(Totals);
		} else {
			var Totals = parseIntZero(getField("Deductions" + sc)[i].Total);
		}
		Totaltotal += Totals;
	}

	setField("D" + sc + "year0", amountFormatter(Totalyear0));
	setField("D" + sc + "year1", amountFormatter(Totalyear1));
	setField("D" + sc + "year2", amountFormatter(Totalyear2));
	setField("D" + sc + "year3", amountFormatter(Totalyear3));
	setField("D" + sc + "year4", amountFormatter(Totalyear4));
	setField("D" + sc + "year5", amountFormatter(Totalyear5));
	setField("D" + sc + "year6", amountFormatter(Totalyear6));
	setField("D" + sc + "year7", amountFormatter(Totalyear7));
	setField("D" + sc + "year8", amountFormatter(Totalyear8));
	setField("D" + sc + "year9", amountFormatter(Totalyear9));
	setField("D" + sc + "year10", amountFormatter(Totalyear10));
	setField("D" + sc + "Total", amountFormatter(Totaltotal));

	//Net Sales
	CalculateNetSales(sc);

	//Gross Margin
	CalculateGrossMargin(sc);

	//Pre Tax Profit
	CalculatePreTaxProfit(sc);

	//AfterTaxProfit
	CalculateAfterTaxProfit(sc);

	updateCFRows();
}



function ExpenseColumnTotal(sc) {
	var TableLength = getField("Expenses" + sc).length;
	var Totalyear0 = 0;
	var Totalyear1 = 0;
	var Totalyear2 = 0;
	var Totalyear3 = 0;
	var Totalyear4 = 0;
	var Totalyear5 = 0;
	var Totalyear6 = 0;
	var Totalyear7 = 0;
	var Totalyear8 = 0;
	var Totalyear9 = 0;
	var Totalyear10 = 0;
	var Totaltotal = 0;
	for (i = 0; i < TableLength; i++) {
		var y0 = getField("Expenses" + sc)[i].Year0.toString();
		var Y0 = y0.indexOf("(");
		if (Y0 == 0) {
			var year0 = getField("Expenses" + sc)[i].Year0.toString();
			year0 = year0.replace(/[()]/g, "");
			year0 = "-" + year0;
			year0 = parseIntZero(year0);
		} else {
			var year0 = parseIntZero(getField("Expenses" + sc)[i].Year0.toString());
		}
		Totalyear0 += year0;
	}
	for (i = 0; i < TableLength; i++) {
		var y1 = getField("Expenses" + sc)[i].Year1.toString();
		var Y1 = y1.indexOf("(");
		if (Y1 == 0) {
			var year1 = getField("Expenses" + sc)[i].Year1.toString();
			year1 = year1.replace(/[()]/g, "");
			year1 = "-" + year1;
			year1 = parseIntZero(year1);
		} else {
			var year1 = parseIntZero(getField("Expenses" + sc)[i].Year1.toString());
		}
		Totalyear1 += year1;
	}
	for (i = 0; i < TableLength; i++) {
		var y2 = getField("Expenses" + sc)[i].Year2.toString();
		var Y2 = y2.indexOf("(");
		if (Y2 == 0) {
			var year2 = getField("Expenses" + sc)[i].Year2.toString();
			year2 = year2.replace(/[()]/g, "");
			year2 = "-" + year2;
			year2 = parseIntZero(year2);
		} else {
			var year2 = parseIntZero(getField("Expenses" + sc)[i].Year2.toString());
		}
		Totalyear2 += year2;
	}
	for (i = 0; i < TableLength; i++) {
		var y3 = getField("Expenses" + sc)[i].Year3.toString();
		var Y3 = y3.indexOf("(");
		if (Y3 == 0) {
			var year3 = getField("Expenses" + sc)[i].Year3.toString();
			year3 = year3.replace(/[()]/g, "");
			year3 = "-" + year3;
			year3 = parseIntZero(year3);
		} else {
			var year3 = parseIntZero(getField("Expenses" + sc)[i].Year3.toString());
		}
		Totalyear3 += year3;
	}
	for (i = 0; i < TableLength; i++) {
		var y4 = getField("Expenses" + sc)[i].Year4.toString();
		var Y4 = y4.indexOf("(");
		if (Y4 == 0) {
			var year4 = getField("Expenses" + sc)[i].Year4.toString();
			year4 = year4.replace(/[()]/g, "");
			year4 = "-" + year4;
			year4 = parseIntZero(year4);
		} else {
			var year4 = parseIntZero(getField("Expenses" + sc)[i].Year4.toString());
		}
		Totalyear4 += year4;
	}
	for (i = 0; i < TableLength; i++) {
		var y5 = getField("Expenses" + sc)[i].Year5.toString();
		var Y5 = y5.indexOf("(");
		if (Y5 == 0) {
			var year5 = getField("Expenses" + sc)[i].Year5.toString();
			year5 = year5.replace(/[()]/g, "");
			year5 = "-" + year5;
			year5 = parseIntZero(year5);
		} else {
			var year5 = parseIntZero(getField("Expenses" + sc)[i].Year5.toString());
		}
		Totalyear5 += year5;
	}
	for (i = 0; i < TableLength; i++) {
		var y6 = getField("Expenses" + sc)[i].Year6.toString();
		var Y6 = y6.indexOf("(");
		if (Y6 == 0) {
			var year6 = getField("Expenses" + sc)[i].Year6.toString();
			year6 = year6.replace(/[()]/g, "");
			year6 = "-" + year6;
			year6 = parseIntZero(year6);
		} else {
			var year6 = parseIntZero(getField("Expenses" + sc)[i].Year6.toString());
		}
		Totalyear6 += year6;
	}
	for (i = 0; i < TableLength; i++) {
		var y7 = getField("Expenses" + sc)[i].Year7.toString();
		var Y7 = y7.indexOf("(");
		if (Y7 == 0) {
			var year7 = getField("Expenses" + sc)[i].Year7.toString();
			year7 = year7.replace(/[()]/g, "");
			year7 = "-" + year7;
			year7 = parseIntZero(year7);
		} else {
			var year7 = parseIntZero(getField("Expenses" + sc)[i].Year7.toString());
		}
		Totalyear7 += year7;
	}
	for (i = 0; i < TableLength; i++) {
		var y8 = getField("Expenses" + sc)[i].Year8.toString();
		var Y8 = y8.indexOf("(");
		if (Y8 == 0) {
			var year8 = getField("Expenses" + sc)[i].Year8.toString();
			year8 = year8.replace(/[()]/g, "");
			year8 = "-" + year8;
			year8 = parseIntZero(year8);
		} else {
			var year8 = parseIntZero(getField("Expenses" + sc)[i].Year8.toString());
		}
		Totalyear8 += year8;
	}
	for (i = 0; i < TableLength; i++) {
		var y9 = getField("Expenses" + sc)[i].Year9.toString();
		var Y9 = y9.indexOf("(");
		if (Y9 == 0) {
			var year9 = getField("Expenses" + sc)[i].Year9.toString();
			year9 = year9.replace(/[()]/g, "");
			year9 = "-" + year9;
			year9 = parseIntZero(year9);
		} else {
			var year9 = parseIntZero(getField("Expenses" + sc)[i].Year9.toString());
		}
		Totalyear9 += year9;
	}
	for (i = 0; i < TableLength; i++) {
		var y10 = getField("Expenses" + sc)[i].Year10.toString();
		var Y10 = y10.indexOf("(");
		if (Y10 == 0) {
			var year10 = getField("Expenses" + sc)[i].Year10.toString();
			year10 = year10.replace(/[()]/g, "");
			year10 = "-" + year10;
			year10 = parseIntZero(year10);
		} else {
			var year10 = parseIntZero(getField("Expenses" + sc)[i].Year10.toString());
		}
		Totalyear10 += year10;
	}
	for (i = 0; i < TableLength; i++) {
		var t = getField("Expenses" + sc)[i].Total.toString();
		var T = t.indexOf("(");
		if (T == 0) {
			var Totals = getField("Expenses" + sc)[i].Total.toString();
			Totals = Totals.replace(/[()]/g, "");
			Totals = "-" + Totals;
			Totals = parseIntZero(Totals);
		} else {
			var Totals = parseIntZero(getField("Expenses" + sc)[i].Total.toString());
		}
		Totaltotal += Totals;
	}
	setField("E" + sc + "year0", amountFormatter(Totalyear0));
	setField("E" + sc + "year1", amountFormatter(Totalyear1));
	setField("E" + sc + "year2", amountFormatter(Totalyear2));
	setField("E" + sc + "year3", amountFormatter(Totalyear3));
	setField("E" + sc + "year4", amountFormatter(Totalyear4));
	setField("E" + sc + "year5", amountFormatter(Totalyear5));
	setField("E" + sc + "year6", amountFormatter(Totalyear6));
	setField("E" + sc + "year7", amountFormatter(Totalyear7));
	setField("E" + sc + "year8", amountFormatter(Totalyear8));
	setField("E" + sc + "year9", amountFormatter(Totalyear9));
	setField("E" + sc + "year10", amountFormatter(Totalyear10));
	setField("E" + sc + "Total", amountFormatter(Totaltotal));

	//Gross Margin
	CalculateGrossMargin(sc);

	//Pre Tax Profit
	CalculatePreTaxProfit(sc);

	//AfterTaxProfit
	CalculateAfterTaxProfit(sc);


	updateCFRows();
}


function OtherExpenseColumnTotal(sc) {

	var TableLength = getField("OtherExpenses" + sc).length;
	var Totalyear0 = 0;
	var Totalyear1 = 0;
	var Totalyear2 = 0;
	var Totalyear3 = 0;
	var Totalyear4 = 0;
	var Totalyear5 = 0;
	var Totalyear6 = 0;
	var Totalyear7 = 0;
	var Totalyear8 = 0;
	var Totalyear9 = 0;
	var Totalyear10 = 0;
	var Totaltotal = 0;
	for (i = 0; i < TableLength; i++) {
		var y0 = getField("OtherExpenses" + sc)[i].Year0;
		var Y0 = y0.indexOf("(");
		if (Y0 == 0) {
			var year0 = getField("OtherExpenses" + sc)[i].Year0;
			year0 = year0.replace(/[()]/g, "");
			year0 = "-" + year0;
			year0 = parseIntZero(year0);
		} else {
			var year0 = parseIntZero(getField("OtherExpenses" + sc)[i].Year0);
		}
		Totalyear0 += year0;
	}
	for (i = 0; i < TableLength; i++) {
		var y1 = getField("OtherExpenses" + sc)[i].Year1;
		var Y1 = y1.indexOf("(");
		if (Y1 == 0) {
			var year1 = getField("OtherExpenses" + sc)[i].Year1;
			year1 = year1.replace(/[()]/g, "");
			year1 = "-" + year1;
			year1 = parseIntZero(year1);
		} else {
			var year1 = parseIntZero(getField("OtherExpenses" + sc)[i].Year1);
		}
		Totalyear1 += year1;
	}
	for (i = 0; i < TableLength; i++) {
		var y2 = getField("OtherExpenses" + sc)[i].Year2;
		var Y2 = y2.indexOf("(")

		if (Y2 == 0) {
			var year2 = getField("OtherExpenses" + sc)[i].Year2;
			year2 = year2.replace(/[()]/g, "");
			year2 = "-" + year2;
			year2 = parseIntZero(year2);
		} else {
			var year2 = parseIntZero(getField("OtherExpenses" + sc)[i].Year2);
		}
		Totalyear2 += year2;
	}
	for (i = 0; i < TableLength; i++) {
		var y3 = getField("OtherExpenses" + sc)[i].Year3;
		var Y3 = y3.indexOf("(")

		if (Y3 == 0) {
			var year3 = getField("OtherExpenses" + sc)[i].Year3;
			year3 = year3.replace(/[()]/g, "");
			year3 = "-" + year3;
			year3 = parseIntZero(year3);
		} else {
			var year3 = parseIntZero(getField("OtherExpenses" + sc)[i].Year3);
		}
		Totalyear3 += year3;
	}
	for (i = 0; i < TableLength; i++) {
		var y4 = getField("OtherExpenses" + sc)[i].Year4;
		var Y4 = y4.indexOf("(")

		if (Y4 == 0) {
			var year4 = getField("OtherExpenses" + sc)[i].Year4;
			year4 = year4.replace(/[()]/g, "");
			year4 = "-" + year4;
			year4 = parseIntZero(year4);
		} else {
			var year4 = parseIntZero(getField("OtherExpenses" + sc)[i].Year4);
		}
		Totalyear4 += year4;
	}
	for (i = 0; i < TableLength; i++) {
		var y5 = getField("OtherExpenses" + sc)[i].Year5;
		var Y5 = y5.indexOf("(")

		if (Y5 == 0) {
			var year5 = getField("OtherExpenses" + sc)[i].Year5;
			year5 = year5.replace(/[()]/g, "");
			year5 = "-" + year5;
			year5 = parseIntZero(year5);
		} else {
			var year5 = parseIntZero(getField("OtherExpenses" + sc)[i].Year5);
		}
		Totalyear5 += year5;
	}
	for (i = 0; i < TableLength; i++) {
		var y6 = getField("OtherExpenses" + sc)[i].Year6;
		var Y6 = y6.indexOf("(")

		if (Y6 == 0) {
			var year6 = getField("OtherExpenses" + sc)[i].Year6;
			year6 = year6.replace(/[()]/g, "");
			year6 = "-" + year6;
			year6 = parseIntZero(year6);
		} else {
			var year6 = parseIntZero(getField("OtherExpenses" + sc)[i].Year6);
		}
		Totalyear6 += year6;
	}
	for (i = 0; i < TableLength; i++) {
		var y7 = getField("OtherExpenses" + sc)[i].Year7;
		var Y7 = y7.indexOf("(")

		if (Y7 == 0) {
			var year7 = getField("OtherExpenses" + sc)[i].Year7;
			year7 = year7.replace(/[()]/g, "");
			year7 = "-" + year7;
			year7 = parseIntZero(year7);
		} else {
			var year7 = parseIntZero(getField("OtherExpenses" + sc)[i].Year7);
		}
		Totalyear7 += year7;
	}
	for (i = 0; i < TableLength; i++) {
		var y8 = getField("OtherExpenses" + sc)[i].Year8;
		var Y8 = y8.indexOf("(")

		if (Y8 == 0) {
			var year8 = getField("OtherExpenses" + sc)[i].Year8;
			year8 = year8.replace(/[()]/g, "");
			year8 = "-" + year8;
			year8 = parseIntZero(year8);
		} else {
			var year8 = parseIntZero(getField("OtherExpenses" + sc)[i].Year8);
		}
		Totalyear8 += year8;
	}
	for (i = 0; i < TableLength; i++) {
		var y9 = getField("OtherExpenses" + sc)[i].Year9;
		var Y9 = y9.indexOf("(")

		if (Y9 == 0) {
			var year9 = getField("OtherExpenses" + sc)[i].Year9;
			year9 = year9.replace(/[()]/g, "");
			year9 = "-" + year9;
			year9 = parseIntZero(year9);
		} else {
			var year9 = parseIntZero(getField("OtherExpenses" + sc)[i].Year9);
		}
		Totalyear9 += year9;
	}
	for (i = 0; i < TableLength; i++) {
		var y10 = getField("OtherExpenses" + sc)[i].Year10;
		var Y10 = y10.indexOf("(")

		if (Y10 == 0) {
			var year10 = getField("OtherExpenses" + sc)[i].Year10;
			year10 = year10.replace(/[()]/g, "");
			year10 = "-" + year10;
			year10 = parseIntZero(year10);
		} else {
			var year10 = parseIntZero(getField("OtherExpenses" + sc)[i].Year10);
		}
		Totalyear10 += year10;
	}
	for (i = 0; i < TableLength; i++) {
		var t = getField("OtherExpenses" + sc)[i].Total;
		var T = t.indexOf("(")

		if (T == 0) {
			var Totals = getField("OtherExpenses" + sc)[i].Total;
			Totals = Totals.replace(/[()]/g, "");
			Totals = "-" + Totals;
			Totals = parseIntZero(Totals);
		} else {
			var Totals = parseIntZero(getField("OtherExpenses" + sc)[i].Total);
		}
		Totaltotal += Totals;
	}
	setField("OE" + sc + "year0", amountFormatter(Totalyear0));
	setField("OE" + sc + "year1", amountFormatter(Totalyear1));
	setField("OE" + sc + "year2", amountFormatter(Totalyear2));
	setField("OE" + sc + "year3", amountFormatter(Totalyear3));
	setField("OE" + sc + "year4", amountFormatter(Totalyear4));
	setField("OE" + sc + "year5", amountFormatter(Totalyear5));
	setField("OE" + sc + "year6", amountFormatter(Totalyear6));
	setField("OE" + sc + "year7", amountFormatter(Totalyear7));
	setField("OE" + sc + "year8", amountFormatter(Totalyear8));
	setField("OE" + sc + "year9", amountFormatter(Totalyear9));
	setField("OE" + sc + "year10", amountFormatter(Totalyear10));
	setField("OE" + sc + "Total", amountFormatter(Totaltotal));

	//Pre Tax Profit
	CalculatePreTaxProfit(sc);

	//AfterTaxProfit
	CalculateAfterTaxProfit(sc);

	updateCFRows();
}

function IEAddingColumns(evt) {

	var y0 = evt.getSource().getParent().getCells()[2].getValue();
	var Y0 = y0.indexOf("(");
	if (Y0 == 0) {
		var year0 = evt.getSource().getParent().getCells()[2].getValue();
		year0 = year0.replace(/[()]/g, "");
		year0 = "-" + year0;
		year0 = parseInt(year0.replace(/\,/g, ''), 10);
	} else {
		var year0 = parseInt((evt.getSource().getParent().getCells()[2].getValue()).replace(/\,/g, ''), 10);
	}
	var Year0 = isNaN(year0);
	if (Year0 === true) {
		year0 = 0;
	}

	var y1 = evt.getSource().getParent().getCells()[3].getValue();
	var Y1 = y1.indexOf("(");
	if (Y1 == 0) {
		var year1 = evt.getSource().getParent().getCells()[3].getValue();
		year1 = year1.replace(/[()]/g, "");
		year1 = "-" + year1;
		year1 = parseInt(year1.replace(/\,/g, ''), 10);
	} else {
		var year1 = parseInt((evt.getSource().getParent().getCells()[3].getValue()).replace(/\,/g, ''), 10);
	}
	var Year1 = isNaN(year1);
	if (Year1 === true) {
		year1 = 0;
	}

	var y2 = evt.getSource().getParent().getCells()[4].getValue();
	var Y2 = y2.indexOf("(");
	if (Y2 == 0) {
		var year2 = evt.getSource().getParent().getCells()[4].getValue();
		year2 = year2.replace(/[()]/g, "");
		year2 = "-" + year2;
		year2 = parseInt(year2.replace(/\,/g, ''), 10);
	} else {
		var year2 = parseInt((evt.getSource().getParent().getCells()[4].getValue()).replace(/\,/g, ''), 10);
	}
	var Year2 = isNaN(year2);
	if (Year2 === true) {
		year2 = 0;
	}

	var y3 = evt.getSource().getParent().getCells()[5].getValue();
	var Y3 = y3.indexOf("(");
	if (Y3 == 0) {
		var year3 = evt.getSource().getParent().getCells()[5].getValue();
		year3 = year3.replace(/[()]/g, "");
		year3 = "-" + year3;
		year3 = parseInt(year3.replace(/\,/g, ''), 10);
	} else {
		var year3 = parseInt((evt.getSource().getParent().getCells()[5].getValue()).replace(/\,/g, ''), 10);
	}
	var Year3 = isNaN(year3);
	if (Year3 === true) {
		year3 = 0;
	}

	var y4 = evt.getSource().getParent().getCells()[6].getValue();
	var Y4 = y4.indexOf("(");
	if (Y4 == 0) {
		var year4 = evt.getSource().getParent().getCells()[6].getValue();
		year4 = year4.replace(/[()]/g, "");
		year4 = "-" + year4;
		year4 = parseInt(year4.replace(/\,/g, ''), 10);
	} else {
		var year4 = parseInt((evt.getSource().getParent().getCells()[6].getValue()).replace(/\,/g, ''), 10);
	}
	var Year4 = isNaN(year4);
	if (Year4 === true) {
		year4 = 0;
	}

	var y5 = evt.getSource().getParent().getCells()[7].getValue();
	var Y5 = y5.indexOf("(");
	if (Y5 == 0) {
		var year5 = evt.getSource().getParent().getCells()[7].getValue();
		year5 = year5.replace(/[()]/g, "");
		year5 = "-" + year5;
		year5 = parseInt(year5.replace(/\,/g, ''), 10);
	} else {
		var year5 = parseInt((evt.getSource().getParent().getCells()[7].getValue()).replace(/\,/g, ''), 10);
	}
	var Year5 = isNaN(year5);
	if (Year5 === true) {
		year5 = 0;
	}

	var y6 = evt.getSource().getParent().getCells()[8].getValue();
	var Y6 = y6.indexOf("(");
	if (Y6 == 0) {
		var year6 = evt.getSource().getParent().getCells()[8].getValue();
		year6 = year6.replace(/[()]/g, "");
		year6 = "-" + year6;
		year6 = parseInt(year6.replace(/\,/g, ''), 10);
	} else {
		var year6 = parseInt((evt.getSource().getParent().getCells()[8].getValue()).replace(/\,/g, ''), 10);
	}
	var Year6 = isNaN(year6);
	if (Year6 === true) {
		year6 = 0;
	}

	var y7 = evt.getSource().getParent().getCells()[9].getValue();
	var Y7 = y7.indexOf("(");
	if (Y7 == 0) {
		var year7 = evt.getSource().getParent().getCells()[9].getValue();
		year7 = year7.replace(/[()]/g, "");
		year7 = "-" + year7;
		year7 = parseInt(year7.replace(/\,/g, ''), 10);
	} else {
		var year7 = parseInt((evt.getSource().getParent().getCells()[9].getValue()).replace(/\,/g, ''), 10);
	}
	var Year7 = isNaN(year7);
	if (Year7 === true) {
		year7 = 0;
	}

	var y8 = evt.getSource().getParent().getCells()[10].getValue();
	var Y8 = y8.indexOf("(");
	if (Y8 == 0) {
		var year8 = evt.getSource().getParent().getCells()[10].getValue();
		year8 = year8.replace(/[()]/g, "");
		year8 = "-" + year8;
		year8 = parseInt(year8.replace(/\,/g, ''), 10);
	} else {
		var year8 = parseInt((evt.getSource().getParent().getCells()[10].getValue()).replace(/\,/g, ''), 10);
	}
	var Year8 = isNaN(year8);
	if (Year8 === true) {
		year8 = 0;
	}

	var y9 = evt.getSource().getParent().getCells()[11].getValue();
	var Y9 = y9.indexOf("(");
	if (Y9 == 0) {
		var year9 = evt.getSource().getParent().getCells()[11].getValue();
		year9 = year9.replace(/[()]/g, "");
		year9 = "-" + year9;
		year9 = parseInt(year9.replace(/\,/g, ''), 10);
	} else {
		var year9 = parseInt((evt.getSource().getParent().getCells()[11].getValue()).replace(/\,/g, ''), 10);
	}
	var Year9 = isNaN(year9);
	if (Year9 === true) {
		year9 = 0;
	}

	var y10 = evt.getSource().getParent().getCells()[12].getValue();
	var Y10 = y10.indexOf("(");
	if (Y10 == 0) {
		var year10 = evt.getSource().getParent().getCells()[12].getValue();
		year10 = year10.replace(/[()]/g, "");
		year10 = "-" + year10;
		year10 = parseInt(year10.replace(/\,/g, ''), 10);
	} else {
		var year10 = parseInt((evt.getSource().getParent().getCells()[12].getValue()).replace(/\,/g, ''), 10);
	}
	var Year10 = isNaN(year10);
	if (Year10 === true) {
		year10 = 0;
	}

	var Total = amountFormatter(year0 + year1 + year2 + year3 + year4 + year5 + year6 + year7 + year8 + year9 + year10);

	evt.getSource().getParent().getCells()[13].setValue(Total);
}
